import 'dart:convert';

import 'package:restaurant_kitchendisplay/utils/all_constans.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserRepository {


  static Future save_userid(String userid,String firstname,String lastname,String isLoginWaiter) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(keyUserid, userid);
    prefs.setString(keyFirstName, firstname);
    prefs.setString(keyLastName, lastname);
    prefs.setString(keyIsLoginWaiter, isLoginWaiter);
  }
  Future<List<String>> getuserdetails()  async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final userid = prefs.getString(keyUserid) ?? "";
    final firstname = prefs.getString(keyFirstName) ?? "";
    final lastname = prefs.getString(keyLastName) ?? "";
    final is_loginwaiter = prefs.getString(keyIsLoginWaiter) ?? "";

    List<String> user_data = [userid,firstname,lastname,is_loginwaiter];

    return user_data;
  }

  static Future save_restaurantid(String restaurantid,String isLogin) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(keyRestaurantId_With_Code, restaurantid);
    prefs.setString(keyIsLogin, isLogin);
  }
  Future<List<String>> getRestaurant_id()  async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final restaurant_id = prefs.getString(keyRestaurantId) ?? "";
    final is_login = prefs.getString(keyIsLogin) ?? "";

    List<String> generaterestautrant_data = [restaurant_id,is_login];
    return generaterestautrant_data;
  }

  static Future save_otp(String restaurant_id) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(keyRestaurantId, restaurant_id);
  }
  Future<List<String>> getGenerateOtpDetails()  async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final restaurant_id = prefs.getString(keyRestaurantId) ?? "";

    List<String> generateotp_data = [restaurant_id];
    return generateotp_data;
  }


  static Future Clearall() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
  }



}