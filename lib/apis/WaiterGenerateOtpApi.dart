import 'dart:convert';

import 'package:requests/requests.dart';

import 'package:http/http.dart' as http;
import 'package:restaurant_kitchendisplay/model/waitergenerateotp.dart';
import 'package:restaurant_kitchendisplay/utils/all_constans.dart';
class WaiterGenerateOtpRepository {
  Future<waitergenerateotpresponse> generateotp(String restaurantCode) async {
    var body = json.encode({'restaurantCode':restaurantCode});
    print(base_url + "generateotp_REQUEST"+ body.toString());

    dynamic response = await Requests.post(base_url + "waiter_generate_restaurant_otp",
        body: body, headers: {'Content-type': 'application/json'});

    waitergenerateotpresponse generateotpresult_data =
    waitergenerateotpresponse.fromJson(jsonDecode(response));
    return generateotpresult_data;
  }


}



