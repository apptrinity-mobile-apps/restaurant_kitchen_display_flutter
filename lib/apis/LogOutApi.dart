import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:restaurant_kitchendisplay/model/loginresponse.dart';
import 'package:restaurant_kitchendisplay/utils/all_constans.dart';
import 'package:http/http.dart' as http;
class LogoutRepository {
  Future<loginresponse> logOut(String restaurantId,String userId, String jobId) async {
    var body = json.encode({'restaurantId':restaurantId,'userId': userId, 'jobId': jobId});
    print(base_url + "LOGOUT" + "LOGOUT_REQUEST"+ body.toString());

    dynamic response = await Requests.post(base_url + "logout",
        body: body, headers: {'Content-type': 'application/json'});


    loginresponse loginresult_data =
    loginresponse.fromJson(jsonDecode(response));
    return loginresult_data;
  }


}


/*
class LoginRepository {
  Future checklogin(String email, String password) async {
    Map<String, dynamic> body = {'email': email, 'password': password};
    print("reviworder----email " + email + " password " + password);

    final response = await http.post(base_url + "userLogin",
        body: body,
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/x-www-form-urlencoded"
        },
        encoding: Encoding.getByName("utf-8"));

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      print("login page " + response.body);
      Map<String, dynamic> user = json.decode(response.body);
      if (user['response'] == true) {
        //print("login page "+user['userId']);

        return user['userId'];
      } else {
        return "failed";
        // print("Failed ");
      }
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }
}*/
