import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/intl.dart';
import 'package:multi_select_item/multi_select_item.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:restaurant_kitchendisplay/session/userRepository.dart';
import 'package:restaurant_kitchendisplay/utils/all_constans.dart';
import 'package:restaurant_kitchendisplay/utils/sizeconfig.dart';
import 'package:toast/toast.dart';

import 'apis/showalldayview.dart';
import 'model/ShowOrdersModel.dart';
import 'model/showalldayview_model.dart';

class ShowRecallOrderScreen extends StatefulWidget {
  @override
  _ShowRecallOrderScreenState createState() => _ShowRecallOrderScreenState();
}

class _ShowRecallOrderScreenState extends State<ShowRecallOrderScreen> {
  int cart_count = 0;
  bool enable_searchlist = false;
  bool _loading = true;
  PageController _controller = new PageController();
  String userid = "";
  String employee_id = "";
  String first_name = "";
  String last_name = "";
  String restaurant_id = "";
  String servicearea_id = "";
  String user_id = "";
  double screenheight = 0.0;
  String returnVal = "";
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  String showalldayview = "paper.png";
  String hidealldayview = "paper_selected.png";
  String showrecentlyfullfilled = "time.png";
  String hiderecentlyfullfilled = "time_selected.png";
  int showalldayhide_status = 0;
  int showrecentlyhide_status = 0;
  int percent = 0;
  int progress_status = 0;
  List<Orders> show_all_orders;
  List<ShowOrders> show_orders = new List();

  @override
  void initState() {
    super.initState();
    //var f = DateFormat('EEE, d MMM yyyy HH:mm:ss');
    var f = new DateFormat("EEE, d MMM yyyy H:m:s")
        .parse('Tue, 20 Jul 2021 10:33:10 GMT');
    var date2 = DateTime.parse(f.toString());
    //date2.toLocal()
    print(date2.toString() + "--" + date2.toLocal().toString());

    UserRepository().getuserdetails().then((userdetails) {
      print("userdata" + userdetails.length.toString());
      setState(() {
        employee_id = userdetails[0];
        first_name = userdetails[1];
        last_name = userdetails[2];
        //user_id = userdetails[1];
        print("SESSIONDATAFROMLOGIN" +
            employee_id +
            "------" +
            first_name +
            "-----" +
            last_name);
        UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
          setState(() {
            restaurant_id = restaurantdetails[0];
            print("restaurant_id" + restaurant_id);
            KitchenRepository()
                .showRecentlyFulfilled(restaurant_id, employee_id)
                .then((result_showOrders) {
              setState(() {
                show_orders = result_showOrders;
                print("result_showOrders" + result_showOrders.toString());
              });
            });
          });
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: login_passcode_bg1));
    print("Height" + SizeConfig.screenHeight.toString());
    if (SizeConfig.screenHeight >= 600 && SizeConfig.screenHeight < 800) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 550 && SizeConfig.screenHeight < 600) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 800) {
      screenheight = 550;
    }
    final screen_height = MediaQuery.of(context).size.height;
    print(screen_height);

    return Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        body: Center(
          child: Container(
              color: Colors.white,
              child: Column(
                children: [
                  Container(
                    color: dashboard_bg,
                    margin: EdgeInsets.all(15),
                    height: screenheight,
                    child: Column(children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.only(left: 10, top: 5),
                              child: new Image.asset(
                                "images/dinning_dashboard.png",
                                height: 48,
                                width: 48,
                              )),
                          Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.only(left: 15, top: 10),
                            child: Text("Food Orders",
                                style: TextStyle(
                                    color: login_passcode_text,
                                    fontSize: 18,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w600)),
                          ),
                        ],
                      ),
                      Expanded(
                        flex: 1,
                        child: Column(children: [
                          Expanded(
                            flex: 1,
                            child: ListView.separated(
                                shrinkWrap: true,
                                physics: ClampingScrollPhysics(),
                                scrollDirection: Axis.vertical,
                                itemCount: show_orders.length,
                                separatorBuilder: (context, index_s) {
                                  return Container(
                                    margin: EdgeInsets.fromLTRB(
                                        15.0, 5.0, 15.0, 0.0),
                                    child: Divider(
                                      color: Colors.white,
                                    ),
                                  );
                                },
                                itemBuilder: (context, index) {
                                  return InkWell(
                                    onTap: () {
                                      setState(() {
                                        gewinner(index);
                                      });
                                    },
                                    child: Card(
                                        margin: EdgeInsets.only(
                                            top: 10,
                                            bottom: 5,
                                            left: 10,
                                            right: 10),
                                        elevation: 5,
                                        child: Container(
                                            color: Colors.white,
                                            child: Container(
                                              child: Column(
                                                children: [
                                                  Container(
                                                    alignment:
                                                        Alignment.topLeft,
                                                    child: Padding(
                                                      padding: EdgeInsets.only(
                                                          top: 10,
                                                          left: 30,
                                                          bottom: 0),
                                                      child: Text(
                                                        show_orders[index]
                                                            .placedFrom
                                                            .toUpperCase(),
                                                        style: TextStyle(
                                                            color:
                                                                dashboard_waiter,
                                                            fontSize: 16,
                                                            fontFamily:
                                                                'Poppins',
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600),
                                                      ),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 10,
                                                        left: 0,
                                                        right: 0),
                                                    color:
                                                    recently_fullfilled,
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      children: [
                                                        Expanded(
                                                            child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Padding(
                                                                padding: EdgeInsets
                                                                    .only(
                                                                        left: 0,
                                                                        bottom:
                                                                            0,
                                                                        top: 5),
                                                                child: Text(
                                                                  "#" +
                                                                      show_orders[
                                                                              index]
                                                                          .checkNumber
                                                                          .toString(),
                                                                  style: TextStyle(
                                                                      color:
                                                                          dashboard_waiter,
                                                                      fontSize:
                                                                          16,
                                                                      fontFamily:
                                                                          'Poppins',
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600),
                                                                  textAlign:
                                                                      TextAlign
                                                                          .start,
                                                                )),
                                                            Padding(
                                                                padding: EdgeInsets
                                                                    .only(
                                                                        left: 0,
                                                                        bottom:
                                                                            5,
                                                                        top: 0),
                                                                child: Text(
                                                                  "Table " +
                                                                      show_orders[
                                                                              index]
                                                                          .tableNumber
                                                                          .toString(),
                                                                  textAlign:
                                                                      TextAlign
                                                                          .start,
                                                                  style: TextStyle(
                                                                      color:
                                                                          dashboard_waiter,
                                                                      fontSize:
                                                                          16,
                                                                      fontFamily:
                                                                          'Poppins',
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600),
                                                                ))
                                                          ],
                                                        )),
                                                        Expanded(
                                                            child: Column(
                                                          children: [
                                                            Padding(
                                                                padding: EdgeInsets
                                                                    .only(
                                                                        left: 0,
                                                                        bottom:
                                                                            0,
                                                                        top: 5),
                                                                child: Text(
                                                                  "G/1",
                                                                  style: TextStyle(
                                                                      color:
                                                                          dashboard_waiter,
                                                                      fontSize:
                                                                          16,
                                                                      fontFamily:
                                                                          'Poppins',
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600),
                                                                ))
                                                          ],
                                                        )),
                                                        Expanded(
                                                            child: Column(
                                                          children: [
                                                            Padding(
                                                                padding: EdgeInsets
                                                                    .only(
                                                                        left: 0,
                                                                        bottom:
                                                                            0,
                                                                        top: 5),
                                                                child: Text(
                                                                    DateTimeConverter(show_orders[
                                                                    index]
                                                                        .created
                                                                        .toString()),
                                                                  textAlign:
                                                                      TextAlign
                                                                          .end,
                                                                  style: TextStyle(
                                                                      color:
                                                                          dashboard_waiter,
                                                                      fontSize:
                                                                          16,
                                                                      fontFamily:
                                                                          'Poppins',
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600),
                                                                )),
                                                            Padding(
                                                                padding: EdgeInsets
                                                                    .only(
                                                                        left: 0,
                                                                        bottom:
                                                                            5,
                                                                        top: 0),
                                                                child: Text(
                                                                  "Max Corn",
                                                                  textAlign:
                                                                      TextAlign
                                                                          .end,
                                                                  style: TextStyle(
                                                                      color:
                                                                          dashboard_waiter,
                                                                      fontSize:
                                                                          16,
                                                                      fontFamily:
                                                                          'Poppins',
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600),
                                                                ))
                                                          ],
                                                        ))
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.fromLTRB(
                                                        0, 10, 0, 10),
                                                    child: ListView.separated(
                                                        shrinkWrap: true,
                                                        physics:
                                                            ClampingScrollPhysics(),
                                                        scrollDirection:
                                                            Axis.vertical,
                                                        itemCount:
                                                            show_orders[index]
                                                                .items
                                                                .length,
                                                        separatorBuilder:
                                                            (context, pindex) {
                                                          return Container(
                                                            margin: EdgeInsets
                                                                .fromLTRB(
                                                                    15.0,
                                                                    0.0,
                                                                    15.0,
                                                                    0.0),
                                                            child: Divider(
                                                              color:
                                                                  Colors.white,
                                                            ),
                                                          );
                                                        },
                                                        itemBuilder:
                                                            (context, pindex) {
                                                          return Container(
                                                            margin: EdgeInsets
                                                                .fromLTRB(15, 0,
                                                                    0, 0),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: <
                                                                  Widget>[
                                                                Expanded(
                                                                    flex: 3,
                                                                    child: Padding(
                                                                        padding: EdgeInsets.only(left: 0, bottom: 0, right: 0),
                                                                        child: Text(
                                                                          show_orders[index]
                                                                              .items[pindex]
                                                                              .name,
                                                                          style: TextStyle(
                                                                              color: coupontext,
                                                                              fontSize: 13,
                                                                              fontFamily: 'Poppins',
                                                                              fontWeight: FontWeight.w600),
                                                                          textAlign:
                                                                              TextAlign.start,
                                                                        ))),
                                                                Expanded(
                                                                    flex: 1,
                                                                    child:
                                                                        Padding(
                                                                      padding: EdgeInsets.only(
                                                                          left:
                                                                              0,
                                                                          bottom:
                                                                              0,
                                                                          right:
                                                                              0),
                                                                      child:
                                                                          Text(
                                                                        show_orders[index]
                                                                            .items[pindex]
                                                                            .quantity
                                                                            .toString(),
                                                                        style: TextStyle(
                                                                            color:
                                                                                coupontext,
                                                                            fontSize:
                                                                                13,
                                                                            fontFamily:
                                                                                'Poppins',
                                                                            fontWeight:
                                                                                FontWeight.w600),
                                                                        textAlign:
                                                                            TextAlign.right,
                                                                      ),
                                                                    )),
                                                                Expanded(
                                                                    flex: 1,
                                                                    child: Padding(
                                                                        padding: EdgeInsets.only(top: 0, bottom: 0, right: 0),
                                                                        child: progress_status == 0
                                                                            ? CircularPercentIndicator(
                                                                                radius: 30.0,
                                                                                lineWidth: 5.0,
                                                                                animation: false,
                                                                                percent: percent / 100,
                                                                                center: new Text(
                                                                                  "10",
                                                                                  style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 8.0),
                                                                                ),
                                                                                circularStrokeCap: CircularStrokeCap.round,
                                                                                progressColor: progressgreen,
                                                                              )
                                                                            : Image.asset(
                                                                                "images/check.png",
                                                                                height: 30,
                                                                                width: 30,
                                                                              ))),
                                                              ],
                                                            ),
                                                          );
                                                        }),
                                                  ),
                                                ],
                                              ),
                                            ))),
                                  );
                                }),
                          ),
                        ]),
                      ),
                    ]),
                  ),
                ],
              )),
        ));
  }

 /* DateTimeConverter(String createddate) {
    print("CREATEDDATE" + createddate.toString());
    var now = DateFormat('E, d MMM yyyy HH:mm:ss').parse(createddate);
    String formattedTime = DateFormat.Hm().format(now);
    print("DATETIME======" + formattedTime);
    return formattedTime.toString();
  }*/

  Future gewinner(int index) async {
    returnVal = await showDialog(
        context: context,
        builder: (context) {
          return _MyDialog(
              user_id: employee_id,
              order_id: show_orders[index].orderId,
              place_From_dialog: show_orders[index].placedFrom,
              check_number_dialog: show_orders[index].checkNumber.toString(),
              table_number_dialog: show_orders[index].tableNumber.toString(),
              created_dialog: show_orders[index].created,
              show_orders_items_dialog: show_orders[index].items);
        });
    print("RETURNVALUEINSIDE" + returnVal);
    if (returnVal == "Success") {
      Future.delayed(Duration(seconds: 2), () async {
        setState(() {});
      });
    }
  }
}

class _MyDialog extends StatefulWidget {
  _MyDialog({
    this.user_id,
    this.order_id,
    this.place_From_dialog,
    this.check_number_dialog,
    this.table_number_dialog,
    this.created_dialog,
    this.show_orders_items_dialog,
  });

  final String user_id;
  final String order_id;
  final String place_From_dialog;
  final String check_number_dialog;
  final String table_number_dialog;
  final String created_dialog;
  List<Items> show_orders_items_dialog = new List();

  @override
  _MyDialogState createState() => _MyDialogState();
}

class _MyDialogState extends State<_MyDialog> {
  var _isChecked = false;
  int percent = 0;
  MultiSelectController controller = new MultiSelectController();
  bool _loading = true;
  PageController _controller = new PageController();
  String userid = "";
  String employee_id = "";
  String first_name = "";
  String last_name = "";
  String restaurant_id = "";
  String servicearea_id = "";
  String user_id = "";
  String item_id = "";
  double screenheight = 0.0;
  int progress_status = 0;
  List<String> item_id_added = new List();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  bool isSelected = false;
  bool selectingmode = false;

  List<Map> data;

  @override
  void initState() {
    super.initState();

    UserRepository().getuserdetails().then((userdetails) {
      print("userdata" + userdetails.length.toString());
      setState(() {
        employee_id = userdetails[0];
        first_name = userdetails[1];
        last_name = userdetails[2];
        //user_id = userdetails[1];
        print("SESSIONDATAFROMLOGIN" +
            employee_id +
            "------" +
            first_name +
            "-----" +
            last_name);
        UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
          setState(() {
            restaurant_id = restaurantdetails[0];
            print("restaurant_id" + restaurant_id);
          });
        });
      });
    });

    /*Timer timer;
    timer = Timer.periodic(Duration(milliseconds: 1000), (_) {
      setState(() {
        percent += 10;
        if (percent >= 100) {
          timer.cancel();
          // percent=0;
        }
      });
    });*/
    data = List.generate(widget.show_orders_items_dialog.length,
        (index) => {'id': index, 'name': 'Item $index', 'isSelected': false});
    // print(widget.main_order_id.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: Colors.transparent,
        insetPadding: EdgeInsets.all(15),
        child: (Column(
          children: [
            Expanded(
                child: SingleChildScrollView(
                    child: Column(
              children: [
                Card(
                    margin:
                        EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
                    elevation: 5,
                    child: Container(
                        color: Colors.white,
                        child: Container(
                          child: Column(
                            children: [
                              Container(
                                alignment: Alignment.topLeft,
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      top: 10, left: 30, bottom: 0),
                                  child: Text(
                                    widget.place_From_dialog,
                                    style: TextStyle(
                                        color: dashboard_waiter,
                                        fontSize: 16,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                              Container(
                                margin:
                                    EdgeInsets.only(top: 10, left: 0, right: 0),
                                color: recently_fullfilled,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Expanded(
                                        child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                            padding: EdgeInsets.only(
                                                left: 0, bottom: 0, top: 5),
                                            child: Text(
                                              "#" + widget.check_number_dialog,
                                              style: TextStyle(
                                                  color: dashboard_waiter,
                                                  fontSize: 16,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600),
                                              textAlign: TextAlign.start,
                                            )),
                                        Padding(
                                            padding: EdgeInsets.only(
                                                left: 0, bottom: 5, top: 0),
                                            child: Text(
                                              "Table " +
                                                  widget.table_number_dialog,
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                  color: dashboard_waiter,
                                                  fontSize: 16,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600),
                                            ))
                                      ],
                                    )),
                                    Expanded(
                                        child: Column(
                                      children: [
                                        Padding(
                                            padding: EdgeInsets.only(
                                                left: 0, bottom: 0, top: 5),
                                            child: Text(
                                              "G/1",
                                              style: TextStyle(
                                                  color: dashboard_waiter,
                                                  fontSize: 16,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600),
                                            ))
                                      ],
                                    )),
                                    Expanded(
                                        child: Column(
                                      children: [
                                        Padding(
                                            padding: EdgeInsets.only(
                                                left: 0, bottom: 0, top: 5),
                                            child: Text(
                                              DateTimeConverter(widget.created_dialog.toString()),
                                              textAlign: TextAlign.end,
                                              style: TextStyle(
                                                  color: dashboard_waiter,
                                                  fontSize: 16,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600),
                                            )),
                                        Padding(
                                            padding: EdgeInsets.only(
                                                left: 0, bottom: 5, top: 0),
                                            child: Text(
                                              "Max Corn",
                                              textAlign: TextAlign.end,
                                              style: TextStyle(
                                                  color: dashboard_waiter,
                                                  fontSize: 16,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600),
                                            ))
                                      ],
                                    ))
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: ListView.separated(
                                    shrinkWrap: true,
                                    physics: ClampingScrollPhysics(),
                                    scrollDirection: Axis.vertical,
                                    itemCount:
                                        widget.show_orders_items_dialog.length,
                                    separatorBuilder: (context, pindex) {
                                      return Container(
                                        margin: EdgeInsets.fromLTRB(
                                            15.0, 0.0, 15.0, 0.0),
                                        child: Divider(
                                          color: Colors.white,
                                        ),
                                      );
                                    },
                                    itemBuilder: (context, pindex) {
                                      return Card(
                                          margin:
                                              EdgeInsets.fromLTRB(5, 0, 5, 0),
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(5)),
                                          // The color depends on this is selected or not
                                          color:
                                              data[pindex]['isSelected'] == true
                                                  ? login_passcode_text
                                                  : Colors.white,
                                          child: ListTile(
                                            onTap: () {
                                              // if this item isn't selected yet, "isSelected": false -> true
                                              // If this item already is selected: "isSelected": true -> false
                                              setState(() {
                                                data[pindex]['isSelected'] =
                                                    !data[pindex]['isSelected'];

                                                print(item_id +
                                                    "------" +
                                                    widget.order_id);
                                              });
                                            },
                                            title:
                                                data[pindex]['isSelected'] ==
                                                        true
                                                    ? Container(
                                                        margin:
                                                            EdgeInsets.fromLTRB(
                                                                15, 0, 0, 0),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: <Widget>[
                                                            Expanded(
                                                                flex: 3,
                                                                child: Padding(
                                                                    padding: EdgeInsets.only(
                                                                        left: 0,
                                                                        bottom:
                                                                            0,
                                                                        right:
                                                                            0),
                                                                    child: Text(
                                                                      widget
                                                                          .show_orders_items_dialog[
                                                                              pindex]
                                                                          .name,
                                                                      style: TextStyle(
                                                                          color: Colors
                                                                              .white,
                                                                          fontSize:
                                                                              13,
                                                                          fontFamily:
                                                                              'Poppins',
                                                                          fontWeight:
                                                                              FontWeight.w600),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .start,
                                                                    ))),
                                                            Expanded(
                                                                flex: 1,
                                                                child: Padding(
                                                                  padding: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              0,
                                                                          bottom:
                                                                              0,
                                                                          right:
                                                                              0),
                                                                  child: Text(
                                                                    widget
                                                                        .show_orders_items_dialog[
                                                                            pindex]
                                                                        .quantity
                                                                        .toString(),
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontSize:
                                                                            13,
                                                                        fontFamily:
                                                                            'Poppins',
                                                                        fontWeight:
                                                                            FontWeight.w600),
                                                                    textAlign:
                                                                        TextAlign
                                                                            .right,
                                                                  ),
                                                                )),
                                                            Expanded(
                                                                flex: 1,
                                                                child: Padding(
                                                                    padding: EdgeInsets.only(
                                                                        top: 0,
                                                                        bottom:
                                                                            0,
                                                                        right:
                                                                            0),
                                                                    child: progress_status ==
                                                                            0
                                                                        ? CircularPercentIndicator(
                                                                            radius:
                                                                                30.0,
                                                                            lineWidth:
                                                                                5.0,
                                                                            animation:
                                                                                true,
                                                                            percent:
                                                                                percent / 100,
                                                                            center:
                                                                                new Text(
                                                                              "10",
                                                                              style: new TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 8.0),
                                                                            ),
                                                                            circularStrokeCap:
                                                                                CircularStrokeCap.round,
                                                                            progressColor:
                                                                                progressgreen,
                                                                          )
                                                                        : Image
                                                                            .asset(
                                                                            "images/check.png",
                                                                            height:
                                                                                30,
                                                                            width:
                                                                                30,
                                                                          ))),
                                                          ],
                                                        ),
                                                      )
                                                    : Container(
                                                        margin:
                                                            EdgeInsets.fromLTRB(
                                                                15, 0, 0, 0),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: <Widget>[
                                                            Expanded(
                                                                flex: 3,
                                                                child: Padding(
                                                                    padding: EdgeInsets.only(
                                                                        left: 0,
                                                                        bottom:
                                                                            0,
                                                                        right:
                                                                            0),
                                                                    child: Text(
                                                                      widget
                                                                          .show_orders_items_dialog[
                                                                              pindex]
                                                                          .name,
                                                                      style: TextStyle(
                                                                          color:
                                                                              coupontext,
                                                                          fontSize:
                                                                              13,
                                                                          fontFamily:
                                                                              'Poppins',
                                                                          fontWeight:
                                                                              FontWeight.w600),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .start,
                                                                    ))),
                                                            Expanded(
                                                                flex: 1,
                                                                child: Padding(
                                                                  padding: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              0,
                                                                          bottom:
                                                                              0,
                                                                          right:
                                                                              0),
                                                                  child: Text(
                                                                    widget
                                                                        .show_orders_items_dialog[
                                                                            pindex]
                                                                        .quantity
                                                                        .toString(),
                                                                    style: TextStyle(
                                                                        color:
                                                                            coupontext,
                                                                        fontSize:
                                                                            13,
                                                                        fontFamily:
                                                                            'Poppins',
                                                                        fontWeight:
                                                                            FontWeight.w600),
                                                                    textAlign:
                                                                        TextAlign
                                                                            .right,
                                                                  ),
                                                                )),
                                                            Expanded(
                                                                flex: 1,
                                                                child: Padding(
                                                                    padding: EdgeInsets.only(
                                                                        top: 0,
                                                                        bottom:
                                                                            0,
                                                                        right:
                                                                            0),
                                                                    child: progress_status ==
                                                                            0
                                                                        ? CircularPercentIndicator(
                                                                            radius:
                                                                                30.0,
                                                                            lineWidth:
                                                                                5.0,
                                                                            animation:
                                                                                true,
                                                                            percent:
                                                                                percent / 100,
                                                                            center:
                                                                                new Text(
                                                                              "10",
                                                                              style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 8.0),
                                                                            ),
                                                                            circularStrokeCap:
                                                                                CircularStrokeCap.round,
                                                                            progressColor:
                                                                                progressgreen,
                                                                          )
                                                                        : Image
                                                                            .asset(
                                                                            "images/check.png",
                                                                            height:
                                                                                30,
                                                                            width:
                                                                                30,
                                                                          ))),
                                                          ],
                                                        ),
                                                      ),
                                          ));
                                    }),
                              ),
                            ],
                          ),
                        ))),

                Container(
                    margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                    height: 50,
                    child: FlatButton(
                        onPressed: () {
                          setState(() {
                            KitchenRepository()
                                .recallOrder(widget.order_id,
                                employee_id, restaurant_id)
                                .then((result) {
                              print("RECALLORDER" + result.toString());
                              Toast.show("RECALLORDER SUCCESSFULLY", context,
                                  duration: Toast.LENGTH_SHORT,
                                  gravity: Toast.BOTTOM);
                              if (result.responseStatus == 0) {

                                Toast.show("No Data Found", context,
                                    duration: Toast.LENGTH_SHORT,
                                    gravity: Toast.BOTTOM);
                              } else {
                                setState(() {
                                  _loading = false;
                                  Navigator.pop(context, "Cancelled");
                                });
                              }
                            });
                          });
                          // Navigator.pop(context, "Cancelled");
                          Toast.show("RECALL", context,
                              duration: Toast.LENGTH_SHORT,
                              gravity: Toast.BOTTOM);
                        },
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    login_passcode_bg1,
                                    login_passcode_bg2
                                  ]),
                              borderRadius: BorderRadius.circular(0)),
                          child: Text("RECALL",
                              style: TextStyle(
                                  fontSize: SizeConfig.safeBlockHorizontal * 4,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w800,
                                  color: Colors.white)),
                        ))),
                Container(
                    margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                    height: 50,
                    child: FlatButton(
                        onPressed: () {
                          Navigator.pop(context, "Cancelled");
                          Toast.show("CANCELLED", context,
                              duration: Toast.LENGTH_SHORT,
                              gravity: Toast.BOTTOM);
                        },
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [cancelgradient1, cancelgradient2]),
                              borderRadius: BorderRadius.circular(0)),
                          child: Text("CANCEL",
                              style: TextStyle(
                                  fontSize: SizeConfig.safeBlockHorizontal * 4,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w800,
                                  color: Colors.white)),
                        ))),

              ],
            )))
          ],
        )));
  }
}

DateTimeConverter(String createddate) {
  print("CREATEDDATE" + createddate.toString());
  DateTime parsedDate = HttpDate.parse(createddate);
  var dateUtcfinal = parsedDate.toUtc();
  var dateutctolocal = dateUtcfinal.toLocal();
  print("PARSEDATELOCAL"+dateUtcfinal.toString()+"========"+dateutctolocal.toString());
  final format = DateFormat('HH:mm');
  final clockString = format.format(dateutctolocal);
  return clockString.toString();
}
