/// modifiers_groups_list : [{"isRequired":1,"itemImage":"http://3.138.149.52/media/menu_groups/60e42342755b314901f9c498.png","maxSelections":5,"minSelections":1,"modifierGroupId":"60e4236e59263dad5bcb64c4","modifierGroupName":"Quench Your Thirst?","modifierGroupPrice":"0.00","modifierList":[{"modifierId":"60e423b8755b314901f9c49b","modifierName":"Coke","price":"3.45"},{"modifierId":"60e423b8755b314901f9c49d","modifierName":"Diet Coke","price":"3.45"},{"modifierId":"60e423b8755b314901f9c49f","modifierName":"Orange Soda","price":"1.44"},{"modifierId":"60e423b8755b314901f9c4a1","modifierName":"RC Cola","price":"1.44"},{"modifierId":"60e423b8755b314901f9c4a3","modifierName":"Sprite","price":"3.45"}],"pricing":1,"quantity":0},{"isRequired":1,"itemImage":"http://3.138.149.52/media/menu_groups/60e42342755b314901f9c498.png","maxSelections":1,"minSelections":0,"modifierGroupId":"60e423d6755b314901f9c4a5","modifierGroupName":"Got a sweet tooth?","modifierGroupPrice":"0.00","modifierList":[{"modifierId":"60e423f559263dad5bcb64ce","modifierName":"Cherish Carrot cake","price":"5.74"}],"pricing":1,"quantity":0},{"isRequired":1,"itemImage":"http://3.138.149.52/media/menu_groups/60e42342755b314901f9c498.png","maxSelections":1,"minSelections":0,"modifierGroupId":"60e4240959263dad5bcb64d2","modifierGroupName":"Choose a Spice Level","modifierGroupPrice":"0.00","modifierList":[{"modifierId":"60e42421755b314901f9c4aa","modifierName":"Hot","price":"0.00"},{"modifierId":"60e42421755b314901f9c4ac","modifierName":"Medium","price":"0.00"},{"modifierId":"60e42421755b314901f9c4ae","modifierName":"Mild","price":"0.00"}],"pricing":1,"quantity":0}]
/// responseStatus : 1
/// result : "Get Data Fetched Successfully!!"

class Samplejsontodart {
  List<Modifiers_groups_list> _modifiersGroupsList;
  int _responseStatus;
  String _result;

  List<Modifiers_groups_list> get modifiersGroupsList => _modifiersGroupsList;
  int get responseStatus => _responseStatus;
  String get result => _result;

  Samplejsontodart({
      List<Modifiers_groups_list> modifiersGroupsList, 
      int responseStatus, 
      String result}){
    _modifiersGroupsList = modifiersGroupsList;
    _responseStatus = responseStatus;
    _result = result;
}

  Samplejsontodart.fromJson(dynamic json) {
    if (json['modifiers_groups_list'] != null) {
      _modifiersGroupsList = [];
      json['modifiers_groups_list'].forEach((v) {
        _modifiersGroupsList.add(Modifiers_groups_list.fromJson(v));
      });
    }
    _responseStatus = json['responseStatus'];
    _result = json['result'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_modifiersGroupsList != null) {
      map['modifiers_groups_list'] = _modifiersGroupsList.map((v) => v.toJson()).toList();
    }
    map['responseStatus'] = _responseStatus;
    map['result'] = _result;
    return map;
  }

}

/// isRequired : 1
/// itemImage : "http://3.138.149.52/media/menu_groups/60e42342755b314901f9c498.png"
/// maxSelections : 5
/// minSelections : 1
/// modifierGroupId : "60e4236e59263dad5bcb64c4"
/// modifierGroupName : "Quench Your Thirst?"
/// modifierGroupPrice : "0.00"
/// modifierList : [{"modifierId":"60e423b8755b314901f9c49b","modifierName":"Coke","price":"3.45"},{"modifierId":"60e423b8755b314901f9c49d","modifierName":"Diet Coke","price":"3.45"},{"modifierId":"60e423b8755b314901f9c49f","modifierName":"Orange Soda","price":"1.44"},{"modifierId":"60e423b8755b314901f9c4a1","modifierName":"RC Cola","price":"1.44"},{"modifierId":"60e423b8755b314901f9c4a3","modifierName":"Sprite","price":"3.45"}]
/// pricing : 1
/// quantity : 0

class Modifiers_groups_list {
  int _isRequired;
  String _itemImage;
  int _maxSelections;
  int _minSelections;
  String _modifierGroupId;
  String _modifierGroupName;
  String _modifierGroupPrice;
  List<ModifierList> _modifierList;
  int _pricing;
  int _quantity;

  int get isRequired => _isRequired;
  String get itemImage => _itemImage;
  int get maxSelections => _maxSelections;
  int get minSelections => _minSelections;
  String get modifierGroupId => _modifierGroupId;
  String get modifierGroupName => _modifierGroupName;
  String get modifierGroupPrice => _modifierGroupPrice;
  List<ModifierList> get modifierList => _modifierList;
  int get pricing => _pricing;
  int get quantity => _quantity;

  Modifiers_groups_list({
      int isRequired, 
      String itemImage, 
      int maxSelections, 
      int minSelections, 
      String modifierGroupId, 
      String modifierGroupName, 
      String modifierGroupPrice, 
      List<ModifierList> modifierList, 
      int pricing, 
      int quantity}){
    _isRequired = isRequired;
    _itemImage = itemImage;
    _maxSelections = maxSelections;
    _minSelections = minSelections;
    _modifierGroupId = modifierGroupId;
    _modifierGroupName = modifierGroupName;
    _modifierGroupPrice = modifierGroupPrice;
    _modifierList = modifierList;
    _pricing = pricing;
    _quantity = quantity;
}

  Modifiers_groups_list.fromJson(dynamic json) {
    _isRequired = json['isRequired'];
    _itemImage = json['itemImage'];
    _maxSelections = json['maxSelections'];
    _minSelections = json['minSelections'];
    _modifierGroupId = json['modifierGroupId'];
    _modifierGroupName = json['modifierGroupName'];
    _modifierGroupPrice = json['modifierGroupPrice'];
    if (json['modifierList'] != null) {
      _modifierList = [];
      json['modifierList'].forEach((v) {
        _modifierList.add(ModifierList.fromJson(v));
      });
    }
    _pricing = json['pricing'];
    _quantity = json['quantity'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['isRequired'] = _isRequired;
    map['itemImage'] = _itemImage;
    map['maxSelections'] = _maxSelections;
    map['minSelections'] = _minSelections;
    map['modifierGroupId'] = _modifierGroupId;
    map['modifierGroupName'] = _modifierGroupName;
    map['modifierGroupPrice'] = _modifierGroupPrice;
    if (_modifierList != null) {
      map['modifierList'] = _modifierList.map((v) => v.toJson()).toList();
    }
    map['pricing'] = _pricing;
    map['quantity'] = _quantity;
    return map;
  }

}

/// modifierId : "60e423b8755b314901f9c49b"
/// modifierName : "Coke"
/// price : "3.45"

class ModifierList {
  String _modifierId;
  String _modifierName;
  String _price;

  String get modifierId => _modifierId;
  String get modifierName => _modifierName;
  String get price => _price;

  ModifierList({
      String modifierId, 
      String modifierName, 
      String price}){
    _modifierId = modifierId;
    _modifierName = modifierName;
    _price = price;
}

  ModifierList.fromJson(dynamic json) {
    _modifierId = json['modifierId'];
    _modifierName = json['modifierName'];
    _price = json['price'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['modifierId'] = _modifierId;
    map['modifierName'] = _modifierName;
    map['price'] = _price;
    return map;
  }

}