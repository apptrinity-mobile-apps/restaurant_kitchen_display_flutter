import 'dart:convert';
import 'dart:developer' as dev;
import 'package:requests/requests.dart';
import 'package:restaurant_kitchendisplay/model/ShowOrdersModel.dart';
import 'package:restaurant_kitchendisplay/model/loginresponse.dart';
import 'package:restaurant_kitchendisplay/model/showalldayview_model.dart';
import 'package:restaurant_kitchendisplay/utils/all_constans.dart';
class KitchenRepository {

  Future<List<Orders>> showalldayview(String restaurantId,String employeeId) async {
    var body = json.encode({'restaurantId':restaurantId,'userId':employeeId});
    print(pos_base_url + "show_all_day_View"+ body.toString());

    dynamic response = await Requests.post(pos_base_url + "show_all_day_view",
        body: body, headers: {'Content-type': 'application/json'});

    print(pos_base_url + "show_all_day_View_response"+ response.toString());
   final res = json.decode(response);
    print("respoNSENSNE--"+ res['orders'].toString());
    Iterable list = res['orders'];
    return list.map((model) => Orders.fromJson(model)).toList();

  }
  Future<List<ShowOrders>> showOrders(String restaurantId,String employeeId) async {
    var body = json.encode({'restaurantId':restaurantId,'userId':employeeId});
    print(pos_base_url + "showOrders"+ body.toString());

    dynamic response = await Requests.post(pos_base_url + "show_orders",
        body: body, headers: {'Content-type': 'application/json'});

    print(pos_base_url + "showOrders_response"+ response.toString());
    final res = json.decode(response);
    print("respoNSENSNE--"+ res['orders'].toString());
    Iterable list = res['orders'];
    return list.map((model) => ShowOrders.fromJson(model)).toList();

  }


  Future<loginresponse> fullfillItem(itemsList, String orderId,String userId,String restaurantId) async {
    var body = json.encode({'itemsList':itemsList,'userId':userId,'orderId':orderId,'restaurantId':restaurantId});
    dev.log(base_url + "FULLFILLORDERREQUEST"+ body.toString());
    dynamic response = await Requests.post(pos_base_url + "fullfill_orders",
        body: body, headers: {'Content-type': 'application/json'});

    loginresponse loginresult_data =
    loginresponse.fromJson(jsonDecode(response));

    print(loginresult_data.result);
    print(loginresult_data.responseStatus);
    return loginresult_data;

  }


  Future<loginresponse> unfullfillItem(itemsList, String orderId,String userId,String restaurantId) async {

    var body = json.encode({'itemsList':itemsList,'userId':userId,'orderId':orderId,'restaurantId':restaurantId});

    dev.log(base_url + "FULLFILLORDERREQUEST"+ body.toString());
    dynamic response = await Requests.post(base_url + "unfullfill_orders",
        body: body, headers: {'Content-type': 'application/json'});

    loginresponse loginresult_data =
    loginresponse.fromJson(jsonDecode(response));

    print(loginresult_data.result);
    print(loginresult_data.responseStatus);
    return loginresult_data;

  }


  Future<List<ShowOrders>> showRecentlyFulfilled(String restaurantId,String employeeId) async {
    var body = json.encode({'restaurantId':restaurantId,'userId':employeeId});
    print(pos_base_url + "show_recently_fullfilled"+ body.toString());

    dynamic response = await Requests.post(pos_base_url + "show_recently_fullfilled",
        body: body, headers: {'Content-type': 'application/json'});

    print(pos_base_url + "show_recently_fullfilled"+ response.toString());
    final res = json.decode(response);
    print("respoNSENSNE--"+ res['orders'].toString());
    Iterable list = res['orders'];
    return list.map((model) => ShowOrders.fromJson(model)).toList();

  }


  Future<loginresponse> recallOrder(String orderId,String userId,String restaurantId) async {
    var body = json.encode({'userId':userId,'orderId':orderId,'restaurantId':restaurantId});
    dev.log(base_url + "update_recall_order_new"+ body.toString());
    dynamic response = await Requests.post(pos_base_url + "update_recall_order_new",
        body: body, headers: {'Content-type': 'application/json'});
    loginresponse loginresult_data =
    loginresponse.fromJson(jsonDecode(response));

    print(loginresult_data.result);
    print(loginresult_data.responseStatus);
    return loginresult_data;

  }


}


