import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/intl.dart';
import 'package:multi_select_item/multi_select_item.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:restaurant_kitchendisplay/restauran_login_with_code.dart';
import 'package:restaurant_kitchendisplay/session/userRepository.dart';
import 'package:restaurant_kitchendisplay/utils/all_constans.dart';
import 'package:restaurant_kitchendisplay/utils/sizeconfig.dart';
import 'package:toast/toast.dart';

import 'apis/LogOutApi.dart';
import 'apis/showalldayview.dart';
import 'appbar.dart';
import 'model/ShowOrdersModel.dart';
import 'model/showalldayview_model.dart';

class ShowAllDay extends StatefulWidget {
  @override
  _ShowAllDayState createState() => _ShowAllDayState();
}

class _ShowAllDayState extends State<ShowAllDay> {
  int cart_count = 0;
  bool enable_searchlist = false;
  bool _loading = true;
  PageController _controller = new PageController();
  String userid = "";
  String employee_id = "";
  String first_name = "";
  String last_name = "";
  String restaurant_id = "";
  String servicearea_id = "";
  String user_id = "";
  double screenheight = 0.0;
  String returnVal = "";
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  String showalldayview = "paper.png";
  String hidealldayview = "paper_selected.png";
  String showrecentlyfullfilled = "time.png";
  String hiderecentlyfullfilled = "time_selected.png";
  int showalldayhide_status = 0;
  int showrecentlyhide_status = 0;
  int percent = 0;
  int progress_status = 0;
  List<Orders> show_all_orders /*= new List()*/;
  /*List<ShowOrders> show_orders = new List();*/

  @override
  void initState() {
    super.initState();
    //var f = DateFormat('EEE, d MMM yyyy HH:mm:ss');
    var f = new DateFormat("EEE, d MMM yyyy H:m:s")
        .parse('Tue, 20 Jul 2021 10:33:10 GMT');
    var date2 = DateTime.parse(f.toString());
    //date2.toLocal()
    print(date2.toString() + "--" + date2.toLocal().toString());

    UserRepository().getuserdetails().then((userdetails) {
      print("userdata" + userdetails.length.toString());
      setState(() {
        employee_id = userdetails[0];
        first_name = userdetails[1];
        last_name = userdetails[2];
        //user_id = userdetails[1];
        print("SESSIONDATAFROMLOGIN" +
            employee_id +
            "------" +
            first_name +
            "-----" +
            last_name);
        UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
          setState(() {
            restaurant_id = restaurantdetails[0];
            print("restaurant_id" + restaurant_id);
            KitchenRepository()
                .showalldayview(restaurant_id, employee_id)
                .then((result_showalldayview) {
              setState(() {
                show_all_orders = result_showalldayview;
              });
            });
          });
        });
      });
    });


  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: login_passcode_bg1));
    print("Height" + SizeConfig.screenHeight.toString());
    if (SizeConfig.screenHeight >= 600 && SizeConfig.screenHeight < 800) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 550 && SizeConfig.screenHeight < 600) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 800) {
      screenheight = 550;
    }
    final screen_height = MediaQuery.of(context).size.height;
    print(screen_height);

    return Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        body: Center(child:  Container(
            color: Colors.white,
            child: Column(
              children: [
                Container(
                  color: dashboard_bg,
                  margin: EdgeInsets.all(15),
                  height: screenheight,
                  child: Column(children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(left: 10, top: 5),
                            child: new Image.asset(
                              "images/dinning_dashboard.png",
                              height: 48,
                              width: 48,
                            )),
                        Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(left: 15, top: 10),
                          child: Text("Food Orders",
                              style: TextStyle(
                                  color: login_passcode_text,
                                  fontSize: 18,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w600)),
                        ),
                      ],
                    ),
                    show_all_orders.length > 0?Expanded(
                        flex: 1,
                        child:Column(children: [
                            Expanded(
                              flex: 1,
                              child: Expanded(
                                      child: Container(
                                          margin: EdgeInsets.only(
                                              left: 0, right: 0),
                                          //padding: EdgeInsets.symmetric(horizontal: 0.0, vertical: 24.0),
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.2,
                                          color: dashboard_bg,
                                          child: ListView.builder(
                                              scrollDirection: Axis.vertical,
                                              itemCount: show_all_orders.length,
                                              itemBuilder: (context, index) {
                                                return Container(
                                                  margin: EdgeInsets.fromLTRB(
                                                      5, 5, 5, 5),
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.3,
                                                  child: InkWell(
                                                      onTap: () {},
                                                      child: Card(
                                                        color: Colors.white,
                                                        child: Container(
                                                          height: 56,
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: <Widget>[
                                                              Expanded(
                                                                flex: 5,
                                                                child: Padding(
                                                                    padding: EdgeInsets
                                                                        .only(
                                                                            left:
                                                                                10),
                                                                    child: Text(
                                                                      show_all_orders[
                                                                              index]
                                                                          .name,
                                                                      style: TextStyle(
                                                                          color:
                                                                              login_passcode_text,
                                                                          fontSize:
                                                                              16,
                                                                          fontFamily:
                                                                              'Poppins',
                                                                          fontWeight:
                                                                              FontWeight.w600),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .start,
                                                                    )),
                                                              ),
                                                              Expanded(
                                                                  flex: 1,
                                                                  child: Padding(
                                                                      padding: EdgeInsets.only(right: 0),
                                                                      child: Text(
                                                                        show_all_orders[index]
                                                                            .quntity
                                                                            .toString(),
                                                                        style: TextStyle(
                                                                            color:
                                                                                login_passcode_text,
                                                                            fontSize:
                                                                                16,
                                                                            fontFamily:
                                                                                'Poppins',
                                                                            fontWeight:
                                                                                FontWeight.w600),
                                                                        textAlign:
                                                                            TextAlign.start,
                                                                      ))),
                                                            ],
                                                          ),
                                                        ),
                                                      )),
                                                );
                                              })),
                                    ),
                            ),
                          ]),
                        ) : Container(
                      margin: EdgeInsets.fromLTRB(0, 100, 0, 0),
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(left: 15, top: 10),
                      child: Text("No Data Found",
                          style: TextStyle(
                              color: login_passcode_text,
                              fontSize: 16,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w600)),
                    ),
                  ]),
                ),
              ],
            )),) );
  }

}



