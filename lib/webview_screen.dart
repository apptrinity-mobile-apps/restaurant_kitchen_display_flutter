import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappbrowser/flutter_inappbrowser.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'appbar_back_arrow.dart';
import 'dashboard.dart';

/*class WebViewExample extends StatefulWidget {
  @override
  WebViewExampleState createState() => WebViewExampleState();
}

class WebViewExampleState extends State<WebViewExample> {
  @override
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();
  var _webViewController;
  String _url;
  //IFrameElement _iframeElement;

  void initState() {
    super.initState();
    // Enable hybrid composition.
    // if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();

    _url = 'https://fts-uat.cardconnect.com/itoke/outer-page.html?usecvv=true&invalidcvvevent=true';
    *//*_iframeElement = IFrameElement()
      ..src = _url
      ..id = 'iframe'
      ..style.border = 'none';
    //ignore: undefined_prefixed_name
    ui.platformViewRegistry.registerViewFactory(
      'iframeElement',
      (int viewId) => _iframeElement,
    );*//*
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: YourAppbarBackArrow(
      title_text: "Payment Screen",
    ),body: WebView(
        initialUrl:
        _url,
        *//*Uri.dataFromString('<html><body><iframe src="https://fts-uat.cardconnect.com/itoke/outer-page.html?invalidinputevent=true"></iframe></body></html>', mimeType: 'text/html').toString(),*//*

        javascriptChannels: Set.from([
          JavascriptChannel(
              name: 'Print',
              onMessageReceived: (JavascriptMessage message) {
                //This is where you receive message from
                //javascript code and handle in Flutter/Dart
                //like here, the message is just being printed
                //in Run/LogCat window of android studio
                print("WEBVIEWRESPONSE"+message.message);
              })
        ]),

        onWebViewCreated: (WebViewController webViewController) {
          //_controller.complete(webViewController);
          _webViewController = webViewController;

        },


       *//* onWebViewCreated: (WebViewController w) {
          _controller = w;
        },*//*


        javascriptMode: JavascriptMode.unrestricted),);


  }
}*/

class WebViewExample extends StatefulWidget {
  @override
  _WebViewWebPageState createState() => _WebViewWebPageState();
}


class _WebViewWebPageState extends State<WebViewExample> {

  // URL to load
  var URL = "www.i1gov.com";
  // Webview progress
  double progress = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Payment Screen"),
        ),
        body: Container(
            child: Column(
                children: <Widget>[
                  (progress != 1.0)
                      ? LinearProgressIndicator(
                      value: progress,
                      backgroundColor: Colors.grey[200],
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.purple))
                      : null,    // Should be removed while showing
                  Expanded(
                    child: Container(
                      child: InAppWebView(
                        initialUrl: URL,
                        initialHeaders: {},
                        initialOptions: {

                        },
                        onWebViewCreated: (InAppWebViewController controller) {
                        },
                        onLoadStart: (InAppWebViewController controller, String url) {
print("URLRESPONSE"+url);
                         /* if(URL == LISTENINGURL) {     // Add your URL here
                            // TODO open native page
                            Navigator.of(context, rootNavigator: true)
                                .push(MaterialPageRoute(
                                builder: (context) => new HomePage()));
                          }*/
                        },
                        onProgressChanged:
                            (InAppWebViewController controller, int progress) {
                          setState(() {
                            this.progress = progress / 100;
                          });
                        },
                      ),
                    ),
                  )
                ].where((Object o) => o != null).toList())));  //Remove null widgets
  }
}







/*class IframeScreen extends StatefulWidget {
  const IframeScreen({Key key}) : super(key: key);

  @override
  _IframeScreenState createState() => _IframeScreenState();
}

class _IframeScreenState extends State<IframeScreen> {
  Widget _iframeWidget;

  final IFrameElement _iframeElement = IFrameElement();

  final _textController = TextEditingController();

  @override
  void initState() {
    super.initState();

    _iframeElement.height = '500';
    _iframeElement.width = '500';

    _iframeElement.src = 'https://www.youtube.com/embed/bYQJp8XQd6U';
    _iframeElement.style.border = 'none';

    // ignore: undefined_prefixed_name
    ui.platformViewRegistry.registerViewFactory(
      'iframeElement',
      (int viewId) => _iframeElement,
    );

    _iframeWidget = HtmlElementView(
      key: UniqueKey(),
      viewType: 'iframeElement',
    );
  }

  @override
  Widget build(BuildContext context) {
    //
    final _width = double.infinity;
    final _height = double.infinity;

    return CustomScaffold(
      titleText: 'Iframe',
      showDrawer: true,
      child: Center(
        child: SizedBox(
          height: _height,
          width: _width,
          child: _iframeWidget,
        ),
      ),
      bottomSheet: WebBottomSheet(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(
                  flex: 2,
                  child: TextField(
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText:
                          'Url please !! (Make sure the url is embed one)',
                      labelText: 'Enter url',
                    ),
                    controller: _textController,
                  ),
                ),
                Flexible(
                  child: FlatButton(
                    color: Colors.grey[800],
                    onPressed: () {
                      final _valid = _isValidURL(_textController.text);
                      if (!_valid) {
                        _showDialog;
                        _textController.text = '';
                      } else {
                        setState(
                            () => _iframeElement.src = _textController.text);
                      }
                    },
                    child: const Text(
                      'Go',
                      style: TextStyle(color: Colors.white),
                    ),
                  )
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  bool _isValidURL(String text) {
    return Uri.parse(text).isAbsolute;
  }

  Future<void> get _showDialog async {
    await showDialog<dynamic>(
      context: context,
      builder: (context) => const GenericDialog(
        children: <Widget>[Text('Wrong URL')],
      ),
    );
  }
}*/
