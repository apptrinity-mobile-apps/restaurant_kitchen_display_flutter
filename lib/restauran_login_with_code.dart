import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:restaurant_kitchendisplay/restauran_login_passcode.dart';
import 'package:restaurant_kitchendisplay/session/userRepository.dart';
import 'package:restaurant_kitchendisplay/utils/DialogClass.dart';
import 'package:restaurant_kitchendisplay/utils/all_constans.dart';
import 'package:restaurant_kitchendisplay/utils/sizeconfig.dart';
import 'package:toast/toast.dart';

import 'apis/WaiterGenerateOtpApi.dart';

class RestauranLoginCode extends StatefulWidget {
  @override
  _RestauranLoginCodeState createState() => _RestauranLoginCodeState();
}

class _RestauranLoginCodeState extends State<RestauranLoginCode> {


  int cart_count = 0;
  double total_qty = 0.0;

  CancelableOperation cancelableOperation;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  int selected_pos = 0;
  String userid = "";
  String res_id = "";
  String menu_id = "";
  TextEditingController restaurant_code_Controller = TextEditingController();
  bool customtip_visible = false;
  final _formKey = GlobalKey<FormState>();
  double logomargin_height;


  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    print("Height"+SizeConfig.screenHeight.toString());
    if(SizeConfig.screenHeight >= 580 && SizeConfig.screenHeight < 800){
      logomargin_height = 120;
    }
    if(SizeConfig.screenHeight >= 800){
      logomargin_height = 220;
    }
    return Scaffold(
      body:
      Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: ExactAssetImage("images/splash_bg.png"),
                fit: BoxFit.cover)),
        child: SingleChildScrollView(child:Column(
          // shrinkWrap: true,
          children: [
            Container(
                margin: EdgeInsets.fromLTRB(40, logomargin_height, 0, 0),
                alignment: Alignment.centerLeft,
                //height: MediaQuery.of(context).size.height * 1.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                      image: AssetImage('images/logo_dashboard.png'),
                      //fit: BoxFit.fitWidth,
                      width: 150,
                      height: 110,
                    ),
                    Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Text(
                          'The Waiter App',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: SizeConfig.blockSizeHorizontal*4,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                          ),
                        ))
                  ],
                )),
            Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                      alignment: Alignment.center,
                      //height: MediaQuery.of(context).size.height * 1.0,
                      child: Column(
                        children: [
                          Container(
                              margin: EdgeInsets.fromLTRB(35, 0, 35, 0),
                              alignment: Alignment.topLeft,
                              child: Text(
                                'Restaurant code',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: SizeConfig.blockSizeHorizontal*3.2,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w400,
                                ),
                              )),
                          Container(
                            margin: EdgeInsets.fromLTRB(35, 5, 35, 0),
                            child: TextFormField(
                              validator: (val) {
                                if (val.isEmpty) return 'Enter Restaurant Code';
                                return null;
                              },
                              controller: restaurant_code_Controller,
                              obscureText: false,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white,
                                  hintText: "Enter Restaurant Code",
                                  hintStyle: TextStyle(color: Colors.grey),
                                  contentPadding: EdgeInsets.only(
                                    bottom: 30 / 2,
                                    left: 50 / 2, // HERE THE IMPORTANT PART
                                    // HERE THE IMPORTANT PART
                                  ),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                            ),
                          )
                        ],
                      )),
                  Container(
                      margin: EdgeInsets.fromLTRB(35, 20, 35, 0),
                      height: 50,
                      width: double.infinity,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(color: login_passcode_bg1, borderRadius: BorderRadius.circular(0)),
                      child: InkWell(
                          child: FlatButton(
                              minWidth: double.infinity,
                              height: double.infinity,
                              child: Text("SUBMIT", style: TextStyle(fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                              onPressed: () {
                                if (_formKey.currentState.validate()) {
                                  _formKey.currentState.save();
                                  SignDialogs.showLoadingDialog(context, "Signing in", _keyLoader);
                                  cancelableOperation?.cancel();
                                  CancelableOperation.fromFuture(Future.delayed(Duration(seconds: 1), () {
                                    WaiterGenerateOtpRepository().generateotp(restaurant_code_Controller.text.toString()).then((value) {
                                      print(value);
                                      if (value.responseStatus == 0) {
                                        Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                        Toast.show("Invalid Credentials", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                      } else {
                                        print("GENERATECODERESPONSE " + value.oTP + "-----" + value.restaurantId + "-----" + value.result);
                                        //UserRepository.save_otp(value.oTP, value.restaurantId);
                                        setState(() {
                                          UserRepository.save_restaurantid(value.restaurantId, "true");
                                          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(builder: (context) => RestauranLoginPaascode(value.oTP, value.restaurantId)),
                                          );
                                        });
                                      }
                                    });
                                  }));
                                }
                              })))
                ],
              ),
            )
          ],
        )),
      ),
    );
  }
}

class CancelDialogs {
  static Future<void> showLoadingDialog(BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(key: key, backgroundColor: Colors.black54, children: <Widget>[
                Center(
                  child: Column(children: [
                    CircularProgressIndicator(),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Cancelling....",
                      style: TextStyle(color: Colors.lightBlueAccent),
                    )
                  ]),
                )
              ]));
        });
  }
}
