import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:restaurant_kitchendisplay/model/loginresponse.dart';
import 'package:restaurant_kitchendisplay/utils/all_constans.dart';

import 'package:http/http.dart' as http;
class LoginRepository {
  Future<loginresponse> checklogin(String restaurantId,String email, String password, String deviceName, String deviceType, String deviceToken) async {
    var body = json.encode({'restaurantId':restaurantId,'email': email, 'password': password, 'deviceName':deviceName, 'deviceType': deviceType, 'deviceToken':deviceToken});
    print(base_url + "login" + "LOGIN_REQUEST"+ body.toString());

    dynamic response = await Requests.post(base_url + "login",
        body: body, headers: {'Content-type': 'application/json'});


    loginresponse loginresult_data =
    loginresponse.fromJson(jsonDecode(response));
    return loginresult_data;
  }

  Future<loginresponse> loginwithpasscode(String restaurantId, String passcode,String deviceName, String deviceType,String deviceToken) async {
    var body = json.encode({'restaurantId':restaurantId,'passcode': passcode, 'deviceName':deviceName, 'deviceType': deviceType, 'deviceToken':deviceToken});
    print(base_url + "login_passcode"+ body.toString());

    dynamic response = await Requests.post(base_url + "login_passcode",
        body: body, headers: {'Content-type': 'application/json'});

    print(base_url + "LOGINRESPONSEWITHCODE"+ response.toString());

    loginresponse loginresult_data =
    loginresponse.fromJson(jsonDecode(response));
    return loginresult_data;
  }
}


/*
class LoginRepository {
  Future checklogin(String email, String password) async {
    Map<String, dynamic> body = {'email': email, 'password': password};
    print("reviworder----email " + email + " password " + password);

    final response = await http.post(base_url + "userLogin",
        body: body,
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/x-www-form-urlencoded"
        },
        encoding: Encoding.getByName("utf-8"));

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      print("login page " + response.body);
      Map<String, dynamic> user = json.decode(response.body);
      if (user['response'] == true) {
        //print("login page "+user['userId']);

        return user['userId'];
      } else {
        return "failed";
        // print("Failed ");
      }
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }
}*/
