class ShowOrdersModel {
  List<ShowOrders> orders;
  int responseStatus;
  String result;

  ShowOrdersModel({this.orders, this.responseStatus, this.result});

  ShowOrdersModel.fromJson(Map<String, dynamic> json) {
    if (json['orders'] != null) {
      orders = new List<ShowOrders>();
      json['orders'].forEach((v) {
        orders.add(new ShowOrders.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.orders != null) {
      data['orders'] = this.orders.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class ShowOrders {
  int checkNumber;
  String created;
  bool isRecallOrder;
  List<Items> items;
  String name;
  String orderId;
  int orderNumber;
  String placedFrom;
  String recallRef;
  int status;
  int tableNumber;

  ShowOrders(
      {this.checkNumber,
        this.created,
        this.isRecallOrder,
        this.items,
        this.name,
        this.orderId,
        this.orderNumber,
        this.placedFrom,
        this.recallRef,
        this.status,
        this.tableNumber});

  ShowOrders.fromJson(Map<String, dynamic> json) {
    checkNumber = json['checkNumber'];
    created = json['created'];
    isRecallOrder = json['isRecallOrder'];
    if (json['items'] != null) {
      items = new List<Items>();
      json['items'].forEach((v) {
        items.add(new Items.fromJson(v));
      });
    }
    name = json['name'];
    orderId = json['orderId'];
    orderNumber = json['orderNumber'];
    placedFrom = json['placedFrom'];
    recallRef = json['recallRef'];
    status = json['status'];
    tableNumber = json['tableNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['checkNumber'] = this.checkNumber;
    data['created'] = this.created;
    data['isRecallOrder'] = this.isRecallOrder;
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    data['name'] = this.name;
    data['orderId'] = this.orderId;
    data['orderNumber'] = this.orderNumber;
    data['placedFrom'] = this.placedFrom;
    data['recallRef'] = this.recallRef;
    data['status'] = this.status;
    data['tableNumber'] = this.tableNumber;
    return data;
  }
}

class Items {
  String id;
  List<ModifiersList> modifiersList;
  String name;
  int preparationTime;
  int quantity;
  int removeQuantity;
  bool removeStatus;
  List<SpecialRequestList> specialRequestList;
  int status;
  int voidQuantity;
  bool voidStatus;

  Items(
      {this.id,
        this.modifiersList,
        this.name,
        this.preparationTime,
        this.quantity,
        this.removeQuantity,
        this.removeStatus,
        this.specialRequestList,
        this.status,
        this.voidQuantity,
        this.voidStatus});

  Items.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    if (json['modifiersList'] != null) {
      modifiersList = new List<ModifiersList>();
      json['modifiersList'].forEach((v) {
        modifiersList.add(new ModifiersList.fromJson(v));
      });
    }
    name = json['name'];
    preparationTime = json['preparationTime'];
    quantity = json['quantity'];
    removeQuantity = json['removeQuantity'];
    removeStatus = json['removeStatus'];
    if (json['specialRequestList'] != null) {
      specialRequestList = new List<SpecialRequestList>();
      json['specialRequestList'].forEach((v) {
        specialRequestList.add(new SpecialRequestList.fromJson(v));
      });
    }
    status = json['status'];
    voidQuantity = json['voidQuantity'];
    voidStatus = json['voidStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.modifiersList != null) {
      data['modifiersList'] =
          this.modifiersList.map((v) => v.toJson()).toList();
    }
    data['name'] = this.name;
    data['preparationTime'] = this.preparationTime;
    data['quantity'] = this.quantity;
    data['removeQuantity'] = this.removeQuantity;
    data['removeStatus'] = this.removeStatus;
    if (this.specialRequestList != null) {
      data['specialRequestList'] =
          this.specialRequestList.map((v) => v.toJson()).toList();
    }
    data['status'] = this.status;
    data['voidQuantity'] = this.voidQuantity;
    data['voidStatus'] = this.voidStatus;
    return data;
  }
}

class ModifiersList {
  String modifierId;
  String modifierName;
  double modifierTotalPrice;
  int modifierQty;
  double modifierUnitPrice;

  ModifiersList(
      {this.modifierId,
        this.modifierName,
        this.modifierTotalPrice,
        this.modifierQty,
        this.modifierUnitPrice});

  ModifiersList.fromJson(Map<String, dynamic> json) {
    modifierId = json['modifierId'];
    modifierName = json['modifierName'];
    modifierTotalPrice = json['modifierTotalPrice'];
    modifierQty = json['modifierQty'];
    modifierUnitPrice = json['modifierUnitPrice'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['modifierId'] = this.modifierId;
    data['modifierName'] = this.modifierName;
    data['modifierTotalPrice'] = this.modifierTotalPrice;
    data['modifierQty'] = this.modifierQty;
    data['modifierUnitPrice'] = this.modifierUnitPrice;
    return data;
  }
}

class SpecialRequestList {
  String name;
  int requestPrice;

  SpecialRequestList({this.name, this.requestPrice});

  SpecialRequestList.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    requestPrice = json['requestPrice'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['requestPrice'] = this.requestPrice;
    return data;
  }
}