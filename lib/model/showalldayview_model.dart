class showalldayview_model {
  List<Orders> orders;
  int responseStatus;
  String result;

  showalldayview_model({this.orders, this.responseStatus, this.result});

  showalldayview_model.fromJson(Map<String, dynamic> json) {
    if (json['orders'] != null) {
      orders = new List<Orders>();
      json['orders'].forEach((v) {
        orders.add(new Orders.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.orders != null) {
      data['orders'] = this.orders.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class Orders {
  String id;
  String name;
  int quntity;

  Orders({this.id, this.name, this.quntity});

  Orders.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    quntity = json['quntity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['quntity'] = this.quntity;
    return data;
  }
}