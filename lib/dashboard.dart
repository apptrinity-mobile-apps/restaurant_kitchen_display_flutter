
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/intl.dart';
import 'package:restaurant_kitchendisplay/restauran_login_with_code.dart';
import 'package:restaurant_kitchendisplay/session/userRepository.dart';
import 'package:restaurant_kitchendisplay/showallday_screen.dart';
import 'package:restaurant_kitchendisplay/showorders_screen.dart';
import 'package:restaurant_kitchendisplay/showrecallorder_screen.dart';
import 'package:restaurant_kitchendisplay/showrecentlyfullfilled_screen.dart';
import 'package:restaurant_kitchendisplay/utils/all_constans.dart';
import 'package:restaurant_kitchendisplay/utils/sizeconfig.dart';
import 'package:toast/toast.dart';

import 'apis/LogOutApi.dart';
import 'appbar.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int cart_count = 0;
  bool enable_searchlist = false;
  bool _loading = true;
  PageController _controller = new PageController();
  String userid = "";
  String employee_id = "";
  String first_name = "";
  String last_name = "";
  String restaurant_id = "";
  String servicearea_id = "";
  String user_id = "";
  double screenheight = 0.0;
  String returnVal = "";
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  String showalldayview = "paper.png";
  String hidealldayview = "paper_selected.png";
  String showrecentlyfullfilled = "time.png";
  String hiderecentlyfullfilled = "time_selected.png";
  String recallorder = "arrow_selected.png";
  String showorder_recall = "time_selected.png";
  int showalldayhide_status = 0;
  int showrecall_status = 0;
  int showrecentlyhide_status = 0;
  int percent = 0;
  int progress_status = 0;

  /*List<Orders> show_all_orders;
  List<ShowOrders> show_orders = new List();*/
  final screens = [ShowOrdersScreen(), ShowAllDay(),ShowRecentlyFullfilledScreen(),ShowRecallOrderScreen()];
  int selected_pos = 0;

  @override
  void initState() {
    super.initState();
    //var f = DateFormat('EEE, d MMM yyyy HH:mm:ss');
    var f = new DateFormat("EEE, d MMM yyyy H:m:s")
        .parse('Tue, 27 Jul 2021 10:24:04 GMT');

    print("UTCTIME"+f.toUtc().toString());
    var date2 = DateTime.parse(f.toString());

    var final_date = date2.toLocal();

    DateTime parsedDate = HttpDate.parse("Tue, 27 Jul 2021 12:45:20 GMT");

    var dateUtcfinal = parsedDate.toUtc();
    var dateutctolocal = dateUtcfinal.toLocal();
    print("PARSEDATELOCAL"+dateUtcfinal.toString()+"========"+dateutctolocal.toString());

    final format = DateFormat('HH:mm');
    final clockString = format.format(dateutctolocal);

    print("DATETIMETOTIME"+clockString.toString());


    //DateTimeConverter(dateutctolocal.toString());



    var dateUtc = DateTime.now().toUtc();
    print("dateUtc: $dateUtc"); // 2019-10-10 12:05:01
// convert it to local
    var dateLocal = dateUtc.toLocal();
    print("local: $dateLocal"); // 2019-10-10 14:05:01

    //date2.toLocal()
    print("DATETIME CONVERTER====="+date2.toString() + "--" + date2.toLocal().toString()+"===="+final_date.toString());
    /*var t = new DateFormat("HH:mm:ss").format(date2);
    print(t);
    DateFormat format = new DateFormat("HH:mm:ss");
    DateTime time = format.parse(t);
    time.toLocal();*/

    //print(time.toLocal());

    /*Timer timer;
    timer = Timer.periodic(Duration(milliseconds: 1000), (_) {
      setState(() {
        percent += 10;
        if (percent >= 100) {
          timer.cancel();
          progress_status == 1;
          // percent=0;
        } else {
          progress_status == 0;
        }
        print("PROGRESSSTATUS" + progress_status.toString());
      });
    });*/
    UserRepository().getuserdetails().then((userdetails) {
      print("userdata" + userdetails.length.toString());
      setState(() {
        employee_id = userdetails[0];
        first_name = userdetails[1];
        last_name = userdetails[2];
        //user_id = userdetails[1];
        print("SESSIONDATAFROMLOGIN" +
            employee_id +
            "------" +
            first_name +
            "-----" +
            last_name);
        UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
          setState(() {
            restaurant_id = restaurantdetails[0];
            print("restaurant_id" + restaurant_id);
            /* KitchenRepository()
                .showOrders(restaurant_id, employee_id)
                .then((result_showOrders) {
              setState(() {
                show_orders = result_showOrders;
                print("result_showOrders" + result_showOrders.toString());
                KitchenRepository()
                    .showalldayview(restaurant_id, employee_id)
                    .then((result_showalldayview) {
                  setState(() {
                    show_all_orders = result_showalldayview;
                  });
                });
              });
            });*/
          });
        });
      });
    });

    /*UserRepository().getuserdetails().then((userdetails) {
      print("userdata" + userdetails.length.toString());
      setState(() {
        employee_id = userdetails[0];
        first_name = userdetails[1];
        last_name = userdetails[2];
        //user_id = userdetails[1];
        print("SESSIONDATAFROMLOGIN" + employee_id + "------" + first_name + "-----" + last_name);
        UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
          setState(() {
            restaurant_id = restaurantdetails[0];
            print("restaurant_id" + restaurant_id);
            KitchenRepository().showalldayview(restaurant_id, employee_id).then((result_showalldayview) {
              setState(() {
                show_all_orders = result_showalldayview;
                print("show_all_orders" + show_all_orders.toString());
              });
            });
          });
        });
      });
    });*/
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: login_passcode_bg1));
    /*SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp]);*/
    //final screen_width = MediaQuery.of(context).size.width;
    print("Height" + SizeConfig.screenHeight.toString());
    if (SizeConfig.screenHeight >= 600 && SizeConfig.screenHeight < 800) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 550 && SizeConfig.screenHeight < 600) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 800) {
      screenheight = 550;
    }
    final screen_height = MediaQuery.of(context).size.height;
    print(screen_height);

    return Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        appBar: YourAppbar(
          height: SizeConfig.safeBlockHorizontal * 25,
          type: "home",
          count: cart_count,
        ),
        drawer: Drawer(
          child: Container(
            color: Colors.white,
            margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: ListView(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
              children: <Widget>[
                Container(
                    color: login_passcode_text,
                    child: DrawerHeader(
                        child: Container(
                            color: login_passcode_text,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(
                                      top: 5, bottom: 5, right: 0),
                                  child: Container(
                                    width: 60.0,
                                    height: 60.0,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: NetworkImage(
                                              'https://i.ibb.co/kSfS1pS/happy-young-waiter-holding-glass-champagne-towel.png')),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(8.0)),
                                      color: login_passcode_text,
                                    ),
                                  ),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                        padding: EdgeInsets.only(right: 15),
                                        child: Text(
                                            first_name + " " + last_name,
                                            style: new TextStyle(
                                                color: cart_text,
                                                fontSize: 18.0,
                                                fontWeight: FontWeight.w700))),
                                    Padding(
                                        padding:
                                            EdgeInsets.only(top: 5, right: 15),
                                        child: Text("#Waiter",
                                            style: new TextStyle(
                                                color: Colors.white,
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.w400)))
                                  ],
                                ),
                              ],
                            )))),
                Container(
                  color: Colors.white,
                  child: Column(
                    children: [
                      ListTile(
                        title: Row(
                          children: [
                            Padding(
                                padding: EdgeInsets.only(right: 15),
                                child: Image.asset(
                                  'images/menu_profile.png',
                                  height: 20,
                                  width: 20,
                                )),
                            RichText(
                                text: TextSpan(children: [
                                  TextSpan(
                                      text: "Dashboard",
                                      style: new TextStyle(
                                          fontSize: 18,
                                          color: login_passcode_text,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w500))
                                ]))
                          ],
                        ),
                        onTap: () {
                           Navigator.push(
                   context,
                   MaterialPageRoute(
                       builder: (context) =>
                           HomePage(
                           )),
                 );
                          // What happens after you tap the navigation item
                        },
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                        child: Divider(
                          color: cart_viewline,
                        ),
                      ),
                      ListTile(
                        title: Row(
                          children: [
                            Padding(
                                padding: EdgeInsets.only(right: 15),
                                child: Image.asset(
                                  'images/menu_profile.png',
                                  height: 20,
                                  width: 20,
                                )),
                            RichText(
                                text: TextSpan(children: [
                              TextSpan(
                                  text: "Profile",
                                  style: new TextStyle(
                                      fontSize: 18,
                                      color: login_passcode_text,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w500))
                            ]))
                          ],
                        ),
                        onTap: () {
                          /* Navigator.push(
                   context,
                   MaterialPageRoute(
                       builder: (context) =>
                           Profile(
                           )),
                 );*/
                          Toast.show("PROFILE", context,
                              duration: Toast.LENGTH_SHORT,
                              gravity: Toast.BOTTOM);
                          // What happens after you tap the navigation item
                        },
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                        child: Divider(
                          color: cart_viewline,
                        ),
                      ),
                      ListTile(
                        title: Row(
                          children: [
                            Padding(
                                padding: EdgeInsets.only(right: 15),
                                child: Image.asset(
                                  'images/menu_orderhistory.png',
                                  height: 20,
                                  width: 20,
                                )),
                            RichText(
                                text: TextSpan(children: [
                              TextSpan(
                                  text: "Order History",
                                  style: new TextStyle(
                                      fontSize: 18,
                                      color: login_passcode_text,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w500))
                            ]))
                          ],
                        ),
                        onTap: () {
                          Toast.show("ORDER HISTORY", context,
                              duration: Toast.LENGTH_SHORT,
                              gravity: Toast.BOTTOM);
                          /*Navigator.push(
                     context,
                     MaterialPageRoute(
                         builder: (context) =>
                             GetOrderList(
                             )),
                   );*/
                          // What happens after you tap the navigation item
                        },
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                        child: Divider(
                          color: cart_viewline,
                        ),
                      ),
                      ListTile(
                        title: Row(
                          children: [
                            Padding(
                                padding: EdgeInsets.only(right: 15),
                                child: Image.asset(
                                  'images/menu_logout.png',
                                  height: 20,
                                  width: 20,
                                )),
                            RichText(
                                text: TextSpan(children: [
                              TextSpan(
                                  text: "Settings",
                                  style: new TextStyle(
                                      fontSize: 18,
                                      color: login_passcode_text,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w500))
                            ]))
                          ],
                        ),
                        onTap: () {
                          Toast.show("Logout", context,
                              duration: Toast.LENGTH_SHORT,
                              gravity: Toast.BOTTOM);
                          // What happens after you tap the navigation item
                        },
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                        child: Divider(
                          color: cart_viewline,
                        ),
                      ),
                      ListTile(
                        title: Row(
                          children: [
                            Padding(
                                padding: EdgeInsets.only(right: 15),
                                child: Image.asset(
                                  'images/menu_logout.png',
                                  height: 20,
                                  width: 20,
                                )),
                            RichText(
                                text: TextSpan(children: [
                              TextSpan(
                                  text: "Log Out",
                                  style: new TextStyle(
                                      fontSize: 18,
                                      color: login_passcode_text,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w500))
                            ]))
                          ],
                        ),
                        onTap: () {
                          LogOut();
                          // What happens after you tap the navigation item
                        },
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        bottomSheet: Container(
          margin: EdgeInsets.fromLTRB(10, 0, 0, 5),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  print("SHOWBUTTONCLICKED" + showalldayhide_status.toString());
                  if (showalldayhide_status == 0) {
                    setState(() {
                      print("SHOWBUTTONINSIDE0" +
                          showalldayhide_status.toString());
                      showalldayhide_status = 1;

                      selected_pos = 1;
                      //  show_orders.clear();
                    });
                  } else {
                    setState(() {
                      print("SHOWBUTTONINSIDE1" +
                          showalldayhide_status.toString());
                      showalldayhide_status = 0;
                      selected_pos = 0;
                      // show_all_orders.clear();
                    });
                  }
                },
                child: Container(
                  width: 130,
                  height: 100,
                  child: Card(
                    color: showalldayhide_status == 0
                        ? Colors.white
                        : login_passcode_text,
                    shape: RoundedRectangleBorder(
                      side: BorderSide(color: login_passcode_bg1, width: 2),
                      borderRadius: BorderRadius.circular(0),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: login_passcode_bg1),
                        borderRadius: BorderRadius.horizontal(
                            left: Radius.circular(0.00), right: Radius.zero),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.only(top: 5, bottom: 0),
                              child: Image.asset(
                                showalldayhide_status == 0
                                    ? 'images/' + hidealldayview
                                    : 'images/' + showalldayview,
                                height: 30,
                                width: 30,
                              )),
                          SizedBox(height: 5),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 10, right: 10, top: 0, bottom: 0),
                              child: Text(showalldayhide_status == 0
                                  ?
                              "SHOW ALL DAY VIEW":"HIDE ALL DAY VIEW",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 12,
                                  color: showalldayhide_status == 0
                                      ? login_passcode_text
                                      : Colors.white,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w600,
                                ),
                              ))
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              InkWell(
                  onTap: () {
                    if (showrecentlyhide_status == 0) {
                      setState(() {
                        print("SHOWBUTTONINSIDE0" +
                            showrecentlyhide_status.toString());
                        showrecentlyhide_status = 1;
                        selected_pos = 2;
                        //  show_orders.clear();
                      });
                    } else {
                      setState(() {
                        print("SHOWBUTTONINSIDE1" +
                            showrecentlyhide_status.toString());
                        showrecentlyhide_status = 0;
                        selected_pos = 0;
                        // show_all_orders.clear();
                      });
                    }

                  },
                  child: Container(
                    width: 130,
                    height: 100,
                    child: Card(
                      color: showrecentlyhide_status == 0
                          ? Colors.white
                          : login_passcode_text,
                      shape: RoundedRectangleBorder(
                        side: BorderSide(color: login_passcode_bg1, width: 2),
                        borderRadius: BorderRadius.circular(0),
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: login_passcode_bg1),
                          borderRadius: BorderRadius.horizontal(
                              left: Radius.circular(0.00), right: Radius.zero),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.only(top: 5, bottom: 0),
                                child: Image.asset(
                                  showrecentlyhide_status == 0
                                      ? 'images/' + hiderecentlyfullfilled
                                      : 'images/' + showrecentlyfullfilled,
                                  height: 30,
                                  width: 30,
                                )),
                            SizedBox(height: 5),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 10, right: 10, top: 0, bottom: 0),
                                child: Text(showrecentlyhide_status == 0
                                    ?
                                "SHOW RECENTLY FULFILLED":"HIDE RECENTLY FULLFILLED",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: showrecentlyhide_status == 0
                                        ? login_passcode_text
                                        : Colors.white,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w600,
                                  ),
                                ))
                          ],
                        ),
                      ),
                    ),
                  )),
              InkWell(
                  onTap: () {
                    setState(() {
                      if (showrecall_status == 0) {
                        showrecall_status = 1;
                        selected_pos = 3;
                      } else {
                        showrecall_status = 0;
                        selected_pos = 0;
                      }
                    });
                  },
                  child: Container(
                    width: 130,
                    height: 100,
                    child: Card(
                        color: showrecall_status == 0
                            ? Colors.white
                            : login_passcode_text,
                      shape: RoundedRectangleBorder(
                        side: BorderSide(color: login_passcode_bg1, width: 2),
                        borderRadius: BorderRadius.circular(0),
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: showrecall_status == 0
                              ?LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [login_passcode_bg1, login_passcode_bg2]):LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [cancelgradient1, dashboard_kitchen_header]),
                          border: Border.all(color: login_passcode_bg1),
                          borderRadius: BorderRadius.horizontal(
                              left: Radius.circular(0.00), right: Radius.zero),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.only(top: 5, bottom: 0),
                                child:
                                Image.asset(
                                  showrecall_status == 0
                                      ? 'images/' + recallorder
                                      : 'images/' + recallorder,
                                  height: 30,
                                  width: 30,
                                )
                              /*Image.asset(
                                  'images/arrow_selected.png',
                                  height: 30,
                                  width: 30,
                                )*/),
                            SizedBox(height: 5),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 10, right: 10, top: 0, bottom: 0),
                                child: Text(
                                  showrecall_status == 0
                                      ?
                                  "RECALL THE ORDER":"SHOW ALL DAY VIEW",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: showrecall_status == 0
                                        ? Colors.white
                                        : Colors.white,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w600,
                                  ),
                                ))
                          ],
                        ),
                      ),
                    ),
                  )),
            ],
          ),
        ),
        body:
            /*Container(
            color: Colors.white,
            child: Column(
              children: [
                Container(
                  color: dashboard_bg,
                  margin: EdgeInsets.all(15),
                  height: screenheight,
                  child: Column(children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(left: 10, top: 10),
                            child: new Image.asset(
                              "images/dinning_dashboard.png",
                              height: 48,
                              width: 48,
                            )),
                        Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(left: 15, top: 10),
                          child: Text("Food Orders",
                              style: TextStyle(
                                  color: login_passcode_text,
                                  fontSize: 16,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w600)),
                        ),
                      ],
                    ),
                    Expanded(
                        flex: 1,
                        child:Column(children: [
                            Expanded(
                              flex: 1,
                              child: showalldayhide_status == 0
                                  ? ListView.separated(
                                      shrinkWrap: true,
                                      physics: ClampingScrollPhysics(),
                                      scrollDirection: Axis.vertical,
                                      itemCount: show_orders.length,
                                      separatorBuilder: (context, index_s) {
                                        return Container(
                                          margin: EdgeInsets.fromLTRB(
                                              15.0, 5.0, 15.0, 0.0),
                                          child: Divider(
                                            color: Colors.white,
                                          ),
                                        );
                                      },
                                      itemBuilder: (context, index) {
                                        return InkWell(
                                          onTap: () {
                                            setState(() {
                                              gewinner(index);
                                            });
                                          },
                                          child: Card(
                                              margin: EdgeInsets.only(
                                                  top: 5,
                                                  bottom: 5,
                                                  left: 10,
                                                  right: 10),
                                              elevation: 5,
                                              child: Container(
                                                  color: Colors.white,
                                                  child: Container(
                                                    child: Column(
                                                      children: [
                                                        Container(
                                                          alignment:
                                                              Alignment.topLeft,
                                                          child: Padding(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    top: 10,
                                                                    left: 30,
                                                                    bottom: 0),
                                                            child: Text(
                                                              show_orders[index]
                                                                  .placedFrom
                                                                  .toUpperCase(),
                                                              style: TextStyle(
                                                                  color:
                                                                      dashboard_waiter,
                                                                  fontSize: 16,
                                                                  fontFamily:
                                                                      'Poppins',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w600),
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  top: 10,
                                                                  left: 0,
                                                                  right: 0),
                                                          color:
                                                              dashboard_kitchen_header,
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Expanded(
                                                                  child: Column(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Padding(
                                                                      padding: EdgeInsets.only(
                                                                          left:
                                                                              0,
                                                                          bottom:
                                                                              0,
                                                                          top:
                                                                              5),
                                                                      child:
                                                                          Text(
                                                                        "#" +
                                                                            show_orders[index].checkNumber.toString(),
                                                                        style: TextStyle(
                                                                            color:
                                                                                dashboard_waiter,
                                                                            fontSize:
                                                                                16,
                                                                            fontFamily:
                                                                                'Poppins',
                                                                            fontWeight:
                                                                                FontWeight.w600),
                                                                        textAlign:
                                                                            TextAlign.start,
                                                                      )),
                                                                  Padding(
                                                                      padding: EdgeInsets.only(
                                                                          left:
                                                                              0,
                                                                          bottom:
                                                                              5,
                                                                          top:
                                                                              0),
                                                                      child:
                                                                          Text(
                                                                        "Table " +
                                                                            show_orders[index].tableNumber.toString(),
                                                                        textAlign:
                                                                            TextAlign.start,
                                                                        style: TextStyle(
                                                                            color:
                                                                                dashboard_waiter,
                                                                            fontSize:
                                                                                16,
                                                                            fontFamily:
                                                                                'Poppins',
                                                                            fontWeight:
                                                                                FontWeight.w600),
                                                                      ))
                                                                ],
                                                              )),
                                                              Expanded(
                                                                  child: Column(
                                                                children: [
                                                                  Padding(
                                                                      padding: EdgeInsets.only(
                                                                          left:
                                                                              0,
                                                                          bottom:
                                                                              0,
                                                                          top:
                                                                              5),
                                                                      child:
                                                                          Text(
                                                                        "G/1",
                                                                        style: TextStyle(
                                                                            color:
                                                                                dashboard_waiter,
                                                                            fontSize:
                                                                                16,
                                                                            fontFamily:
                                                                                'Poppins',
                                                                            fontWeight:
                                                                                FontWeight.w600),
                                                                      ))
                                                                ],
                                                              )),
                                                              Expanded(
                                                                  child: Column(
                                                                children: [
                                                                  Padding(
                                                                      padding: EdgeInsets.only(
                                                                          left:
                                                                              0,
                                                                          bottom:
                                                                              0,
                                                                          top:
                                                                              5),
                                                                      child:
                                                                          Text(
                                                                        show_orders[index]
                                                                            .created
                                                                            .toString(),
                                                                        textAlign:
                                                                            TextAlign.end,
                                                                        style: TextStyle(
                                                                            color:
                                                                                dashboard_waiter,
                                                                            fontSize:
                                                                                16,
                                                                            fontFamily:
                                                                                'Poppins',
                                                                            fontWeight:
                                                                                FontWeight.w600),
                                                                      )),
                                                                  Padding(
                                                                      padding: EdgeInsets.only(
                                                                          left:
                                                                              0,
                                                                          bottom:
                                                                              5,
                                                                          top:
                                                                              0),
                                                                      child:
                                                                          Text(
                                                                        "Max Corn",
                                                                        textAlign:
                                                                            TextAlign.end,
                                                                        style: TextStyle(
                                                                            color:
                                                                                dashboard_waiter,
                                                                            fontSize:
                                                                                16,
                                                                            fontFamily:
                                                                                'Poppins',
                                                                            fontWeight:
                                                                                FontWeight.w600),
                                                                      ))
                                                                ],
                                                              ))
                                                            ],
                                                          ),
                                                        ),
                                                        Container(
                                                          margin: EdgeInsets
                                                              .fromLTRB(
                                                                  0, 10, 0, 10),
                                                          child: ListView
                                                              .separated(
                                                                  shrinkWrap:
                                                                      true,
                                                                  physics:
                                                                      ClampingScrollPhysics(),
                                                                  scrollDirection:
                                                                      Axis
                                                                          .vertical,
                                                                  itemCount:
                                                                      show_orders[
                                                                              index]
                                                                          .items
                                                                          .length,
                                                                  separatorBuilder:
                                                                      (context,
                                                                          pindex) {
                                                                    return Container(
                                                                      margin: EdgeInsets.fromLTRB(
                                                                          15.0,
                                                                          0.0,
                                                                          15.0,
                                                                          0.0),
                                                                      child:
                                                                          Divider(
                                                                        color: Colors
                                                                            .white,
                                                                      ),
                                                                    );
                                                                  },
                                                                  itemBuilder:
                                                                      (context,
                                                                          pindex) {
                                                                    return Container(
                                                                      margin: EdgeInsets
                                                                          .fromLTRB(
                                                                              15,
                                                                              0,
                                                                              0,
                                                                              0),
                                                                      child:
                                                                          Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.spaceBetween,
                                                                        children: <
                                                                            Widget>[
                                                                          Expanded(
                                                                              flex: 3,
                                                                              child: Padding(
                                                                                  padding: EdgeInsets.only(left: 0, bottom: 0, right: 0),
                                                                                  child: Text(
                                                                                    show_orders[index].items[pindex].name,
                                                                                    style: TextStyle(color: coupontext, fontSize: 13, fontFamily: 'Poppins', fontWeight: FontWeight.w600),
                                                                                    textAlign: TextAlign.start,
                                                                                  ))),
                                                                          Expanded(
                                                                              flex: 1,
                                                                              child: Padding(
                                                                                padding: EdgeInsets.only(left: 0, bottom: 0, right: 0),
                                                                                child: Text(
                                                                                  show_orders[index].items[pindex].quantity.toString(),
                                                                                  style: TextStyle(color: coupontext, fontSize: 13, fontFamily: 'Poppins', fontWeight: FontWeight.w600),
                                                                                  textAlign: TextAlign.right,
                                                                                ),
                                                                              )),
                                                                          Expanded(
                                                                              flex: 1,
                                                                              child: Padding(
                                                                                  padding: EdgeInsets.only(top: 0, bottom: 0, right: 0),
                                                                                  child: progress_status == 0
                                                                                      ? CircularPercentIndicator(
                                                                                          radius: 30.0,
                                                                                          lineWidth: 5.0,
                                                                                          animation: true,
                                                                                          percent: percent / 100,
                                                                                          center: new Text(
                                                                                            "10",
                                                                                            style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 8.0),
                                                                                          ),
                                                                                          circularStrokeCap: CircularStrokeCap.round,
                                                                                          progressColor: progressgreen,
                                                                                        )
                                                                                      : Image.asset(
                                                                                          "images/check.png",
                                                                                          height: 30,
                                                                                          width: 30,
                                                                                        ))),
                                                                        ],
                                                                      ),
                                                                    );
                                                                  }),
                                                        ),
                                                      ],
                                                    ),
                                                  ))),
                                        );
                                      })
                                  : Expanded(
                                      child: Container(
                                          margin: EdgeInsets.only(
                                              left: 0, right: 0),
                                          //padding: EdgeInsets.symmetric(horizontal: 0.0, vertical: 24.0),
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.2,
                                          color: dashboard_bg,
                                          child: ListView.builder(
                                              scrollDirection: Axis.vertical,
                                              itemCount: show_all_orders.length,
                                              itemBuilder: (context, index) {
                                                return Container(
                                                  margin: EdgeInsets.fromLTRB(
                                                      5, 0, 5, 5),
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.3,
                                                  child: InkWell(
                                                      onTap: () {},
                                                      child: Card(
                                                        color: Colors.white,
                                                        child: Container(
                                                          height: 56,
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: <Widget>[
                                                              Expanded(
                                                                flex: 5,
                                                                child: Padding(
                                                                    padding: EdgeInsets
                                                                        .only(
                                                                            left:
                                                                                10),
                                                                    child: Text(
                                                                      show_all_orders[
                                                                              index]
                                                                          .name,
                                                                      style: TextStyle(
                                                                          color:
                                                                              login_passcode_text,
                                                                          fontSize:
                                                                              16,
                                                                          fontFamily:
                                                                              'Poppins',
                                                                          fontWeight:
                                                                              FontWeight.w600),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .start,
                                                                    )),
                                                              ),
                                                              Expanded(
                                                                  flex: 1,
                                                                  child: Padding(
                                                                      padding: EdgeInsets.only(right: 0),
                                                                      child: Text(
                                                                        show_all_orders[index]
                                                                            .quntity
                                                                            .toString(),
                                                                        style: TextStyle(
                                                                            color:
                                                                                login_passcode_text,
                                                                            fontSize:
                                                                                16,
                                                                            fontFamily:
                                                                                'Poppins',
                                                                            fontWeight:
                                                                                FontWeight.w600),
                                                                        textAlign:
                                                                            TextAlign.start,
                                                                      ))),
                                                            ],
                                                          ),
                                                        ),
                                                      )),
                                                );
                                              })),
                                    ),
                            ),
                          ]),
                        ),
                  ]),
                ),
              ],
            ))*/
            screens[selected_pos]);
  }

  DateTimeConverter(String createddate) {
    print("CREATEDDATE" + createddate.toString());
    var now = DateFormat('yyyy dd MM HH:mm:ss:ms').parse(createddate);
    String formattedTime = DateFormat.Hm().format(now);
    print("DATETIME======" + formattedTime);
    return formattedTime.toString();
  }

  LogOut() {
    LogoutRepository()
        .logOut(restaurant_id, employee_id, "5f894acdb7c2fbf9edf286ff")
        .then((value) {
      print(value);
      if (value.responseStatus == 0) {
        Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
        Toast.show("Invalid Credentials", context,
            duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
      } else if (value.responseStatus == 1) {
        print("LOGOUTRESPONSE " +
            "--" +
            value.result +
            "--" +
            value.responseStatus.toString());
        setState(() {
          /*Navigator
                      .of(
                      _keyLoader
                          .currentContext,
                      rootNavigator: true).pop();*/
          Toast.show(value.result, context,
              duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
          UserRepository.Clearall();
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => RestauranLoginCode()),
          );
        });
      }
    });
  }

/*Future gewinner(int index) async {
    returnVal = await showDialog(
        context: context,
        builder: (context) {
          return _MyDialog(
              user_id: employee_id,
              order_id: show_orders[index].orderId,
              place_From_dialog: show_orders[index].placedFrom,
              check_number_dialog: show_orders[index].checkNumber.toString(),
              table_number_dialog: show_orders[index].tableNumber.toString(),
              created_dialog: show_orders[index].created,
              show_orders_items_dialog: show_orders[index].items);
        });
    print("RETURNVALUEINSIDE" + returnVal);
    if (returnVal == "Success") {
      Future.delayed(Duration(seconds: 2), () async {
        setState(() {});
      });
    }
  }*/
}

/*class _MyDialog extends StatefulWidget {
  _MyDialog({
    this.user_id,
    this.order_id,
    this.place_From_dialog,
    this.check_number_dialog,
    this.table_number_dialog,
    this.created_dialog,
    this.show_orders_items_dialog,
  });

  final String user_id;
  final String order_id;
  final String place_From_dialog;
  final String check_number_dialog;
  final String table_number_dialog;
  final String created_dialog;
  List<Items> show_orders_items_dialog = new List();

  @override
  _MyDialogState createState() => _MyDialogState();
}

class _MyDialogState extends State<_MyDialog> {
  var _isChecked = false;
  int percent = 0;
  MultiSelectController controller = new MultiSelectController();
  bool _loading = true;
  PageController _controller = new PageController();
  String userid = "";
  String employee_id = "";
  String first_name = "";
  String last_name = "";
  String restaurant_id = "";
  String servicearea_id = "";
  String user_id = "";
  String item_id = "";
  double screenheight = 0.0;
  int progress_status = 0;
  List<String> item_id_added = new List();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  bool isSelected = false;
  bool selectingmode = false;

  List<Map> data;

  @override
  void initState() {
    super.initState();

    UserRepository().getuserdetails().then((userdetails) {
      print("userdata" + userdetails.length.toString());
      setState(() {
        employee_id = userdetails[0];
        first_name = userdetails[1];
        last_name = userdetails[2];
        //user_id = userdetails[1];
        print("SESSIONDATAFROMLOGIN" +
            employee_id +
            "------" +
            first_name +
            "-----" +
            last_name);
        UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
          setState(() {
            restaurant_id = restaurantdetails[0];
            print("restaurant_id" + restaurant_id);
          });
        });
      });
    });

    Timer timer;
    timer = Timer.periodic(Duration(milliseconds: 1000), (_) {
      setState(() {
        percent += 10;
        if (percent >= 100) {
          timer.cancel();
          // percent=0;
        }
      });
    });
    data = List.generate(widget.show_orders_items_dialog.length,
        (index) => {'id': index, 'name': 'Item $index', 'isSelected': false});
    // print(widget.main_order_id.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: Colors.transparent,
        insetPadding: EdgeInsets.all(15),
        child: (Column(
          children: [
            Expanded(
                child: SingleChildScrollView(
                    child: Column(
              children: [
                Card(
                    margin:
                        EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
                    elevation: 5,
                    child: Container(
                        color: Colors.white,
                        child: Container(
                          child: Column(
                            children: [
                              Container(
                                alignment: Alignment.topLeft,
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      top: 10, left: 30, bottom: 0),
                                  child: Text(
                                    widget.place_From_dialog,
                                    style: TextStyle(
                                        color: dashboard_waiter,
                                        fontSize: 16,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                              Container(
                                margin:
                                    EdgeInsets.only(top: 10, left: 0, right: 0),
                                color: dashboard_kitchen_header,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Expanded(
                                        child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                            padding: EdgeInsets.only(
                                                left: 0, bottom: 0, top: 5),
                                            child: Text(
                                              "#" + widget.check_number_dialog,
                                              style: TextStyle(
                                                  color: dashboard_waiter,
                                                  fontSize: 16,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600),
                                              textAlign: TextAlign.start,
                                            )),
                                        Padding(
                                            padding: EdgeInsets.only(
                                                left: 0, bottom: 5, top: 0),
                                            child: Text(
                                              "Table " +
                                                  widget.table_number_dialog,
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                  color: dashboard_waiter,
                                                  fontSize: 16,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600),
                                            ))
                                      ],
                                    )),
                                    Expanded(
                                        child: Column(
                                      children: [
                                        Padding(
                                            padding: EdgeInsets.only(
                                                left: 0, bottom: 0, top: 5),
                                            child: Text(
                                              "G/1",
                                              style: TextStyle(
                                                  color: dashboard_waiter,
                                                  fontSize: 16,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600),
                                            ))
                                      ],
                                    )),
                                    Expanded(
                                        child: Column(
                                      children: [
                                        Padding(
                                            padding: EdgeInsets.only(
                                                left: 0, bottom: 0, top: 5),
                                            child: Text(
                                              widget.created_dialog,
                                              textAlign: TextAlign.end,
                                              style: TextStyle(
                                                  color: dashboard_waiter,
                                                  fontSize: 16,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600),
                                            )),
                                        Padding(
                                            padding: EdgeInsets.only(
                                                left: 0, bottom: 5, top: 0),
                                            child: Text(
                                              "Max Corn",
                                              textAlign: TextAlign.end,
                                              style: TextStyle(
                                                  color: dashboard_waiter,
                                                  fontSize: 16,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600),
                                            ))
                                      ],
                                    ))
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: ListView.separated(
                                    shrinkWrap: true,
                                    physics: ClampingScrollPhysics(),
                                    scrollDirection: Axis.vertical,
                                    itemCount:
                                        widget.show_orders_items_dialog.length,
                                    separatorBuilder: (context, pindex) {
                                      return Container(
                                        margin: EdgeInsets.fromLTRB(
                                            15.0, 0.0, 15.0, 0.0),
                                        child: Divider(
                                          color: Colors.white,
                                        ),
                                      );
                                    },
                                    itemBuilder: (context, pindex) {
                                      return Card(
                                          margin:
                                              EdgeInsets.fromLTRB(5, 0, 5, 0),
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(5)),
                                          // The color depends on this is selected or not
                                          color:
                                              data[pindex]['isSelected'] == true
                                                  ? login_passcode_text
                                                  : Colors.white,
                                          child: ListTile(
                                            onTap: () {
                                              // if this item isn't selected yet, "isSelected": false -> true
                                              // If this item already is selected: "isSelected": true -> false
                                              setState(() {
                                                data[pindex]['isSelected'] =
                                                    !data[pindex]['isSelected'];

                                                print(item_id +
                                                    "------" +
                                                    widget.order_id);
                                              });
                                            },
                                            title:
                                                data[pindex]['isSelected'] ==
                                                        true
                                                    ? Container(
                                                        margin:
                                                            EdgeInsets.fromLTRB(
                                                                15, 0, 0, 0),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: <Widget>[
                                                            Expanded(
                                                                flex: 3,
                                                                child: Padding(
                                                                    padding: EdgeInsets.only(
                                                                        left: 0,
                                                                        bottom:
                                                                            0,
                                                                        right:
                                                                            0),
                                                                    child: Text(
                                                                      widget
                                                                          .show_orders_items_dialog[
                                                                              pindex]
                                                                          .name,
                                                                      style: TextStyle(
                                                                          color: Colors
                                                                              .white,
                                                                          fontSize:
                                                                              13,
                                                                          fontFamily:
                                                                              'Poppins',
                                                                          fontWeight:
                                                                              FontWeight.w600),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .start,
                                                                    ))),
                                                            Expanded(
                                                                flex: 1,
                                                                child: Padding(
                                                                  padding: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              0,
                                                                          bottom:
                                                                              0,
                                                                          right:
                                                                              0),
                                                                  child: Text(
                                                                    widget
                                                                        .show_orders_items_dialog[
                                                                            pindex]
                                                                        .quantity
                                                                        .toString(),
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontSize:
                                                                            13,
                                                                        fontFamily:
                                                                            'Poppins',
                                                                        fontWeight:
                                                                            FontWeight.w600),
                                                                    textAlign:
                                                                        TextAlign
                                                                            .right,
                                                                  ),
                                                                )),
                                                            Expanded(
                                                                flex: 1,
                                                                child: Padding(
                                                                    padding: EdgeInsets.only(
                                                                        top: 0,
                                                                        bottom:
                                                                            0,
                                                                        right:
                                                                            0),
                                                                    child: progress_status ==
                                                                            0
                                                                        ? CircularPercentIndicator(
                                                                            radius:
                                                                                30.0,
                                                                            lineWidth:
                                                                                5.0,
                                                                            animation:
                                                                                true,
                                                                            percent:
                                                                                percent / 100,
                                                                            center:
                                                                                new Text(
                                                                              "10",
                                                                              style: new TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 8.0),
                                                                            ),
                                                                            circularStrokeCap:
                                                                                CircularStrokeCap.round,
                                                                            progressColor:
                                                                                progressgreen,
                                                                          )
                                                                        : Image
                                                                            .asset(
                                                                            "images/check.png",
                                                                            height:
                                                                                30,
                                                                            width:
                                                                                30,
                                                                          ))),
                                                          ],
                                                        ),
                                                      )
                                                    : Container(
                                                        margin:
                                                            EdgeInsets.fromLTRB(
                                                                15, 0, 0, 0),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: <Widget>[
                                                            Expanded(
                                                                flex: 3,
                                                                child: Padding(
                                                                    padding: EdgeInsets.only(
                                                                        left: 0,
                                                                        bottom:
                                                                            0,
                                                                        right:
                                                                            0),
                                                                    child: Text(
                                                                      widget
                                                                          .show_orders_items_dialog[
                                                                              pindex]
                                                                          .name,
                                                                      style: TextStyle(
                                                                          color:
                                                                              coupontext,
                                                                          fontSize:
                                                                              13,
                                                                          fontFamily:
                                                                              'Poppins',
                                                                          fontWeight:
                                                                              FontWeight.w600),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .start,
                                                                    ))),
                                                            Expanded(
                                                                flex: 1,
                                                                child: Padding(
                                                                  padding: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              0,
                                                                          bottom:
                                                                              0,
                                                                          right:
                                                                              0),
                                                                  child: Text(
                                                                    widget
                                                                        .show_orders_items_dialog[
                                                                            pindex]
                                                                        .quantity
                                                                        .toString(),
                                                                    style: TextStyle(
                                                                        color:
                                                                            coupontext,
                                                                        fontSize:
                                                                            13,
                                                                        fontFamily:
                                                                            'Poppins',
                                                                        fontWeight:
                                                                            FontWeight.w600),
                                                                    textAlign:
                                                                        TextAlign
                                                                            .right,
                                                                  ),
                                                                )),
                                                            Expanded(
                                                                flex: 1,
                                                                child: Padding(
                                                                    padding: EdgeInsets.only(
                                                                        top: 0,
                                                                        bottom:
                                                                            0,
                                                                        right:
                                                                            0),
                                                                    child: progress_status ==
                                                                            0
                                                                        ? CircularPercentIndicator(
                                                                            radius:
                                                                                30.0,
                                                                            lineWidth:
                                                                                5.0,
                                                                            animation:
                                                                                true,
                                                                            percent:
                                                                                percent / 100,
                                                                            center:
                                                                                new Text(
                                                                              "10",
                                                                              style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 8.0),
                                                                            ),
                                                                            circularStrokeCap:
                                                                                CircularStrokeCap.round,
                                                                            progressColor:
                                                                                progressgreen,
                                                                          )
                                                                        : Image
                                                                            .asset(
                                                                            "images/check.png",
                                                                            height:
                                                                                30,
                                                                            width:
                                                                                30,
                                                                          ))),
                                                          ],
                                                        ),
                                                      ),
                                          ));
                                    }),
                              ),
                            ],
                          ),
                        ))),
                Container(
                    margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                    height: 50,
                    child: FlatButton(
                        onPressed: () {
                          Navigator.pop(context, "Cancelled");
                          Toast.show("CANCELLED", context,
                              duration: Toast.LENGTH_SHORT,
                              gravity: Toast.BOTTOM);
                        },
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [cancelgradient1, cancelgradient2]),
                              borderRadius: BorderRadius.circular(0)),
                          child: Text("CANCEL",
                              style: TextStyle(
                                  fontSize: SizeConfig.safeBlockHorizontal * 4,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w800,
                                  color: Colors.white)),
                        ))),
                Container(
                    margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                    height: 50,
                    child: FlatButton(
                        onPressed: () {
                          setState(() {
                            for (var i = 0; i < data.length; i++) {
                              data[i]['isSelected'] = true;
                            }
                          });

                          // Navigator.pop(context, "Cancelled");
                          Toast.show("SELECTED ALL", context,
                              duration: Toast.LENGTH_SHORT,
                              gravity: Toast.BOTTOM);
                        },
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    login_passcode_bg1,
                                    login_passcode_bg2
                                  ]),
                              borderRadius: BorderRadius.circular(0)),
                          child: Text("SELECT ALL",
                              style: TextStyle(
                                  fontSize: SizeConfig.safeBlockHorizontal * 4,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w800,
                                  color: Colors.white)),
                        ))),
                Container(
                    margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                    height: 50,
                    child: FlatButton(
                        onPressed: () {
                          print("DATALENGTH" + data.length.toString());
                          item_id_added.clear();
                          for (int i = 0; i < data.length; i++) {
                            *//* int remove_pos = data.indexOf(true);
                                    all_checks[previous_tab_pos].removeAt(remove_pos);*//*
                            print(data[i]['isSelected']);
                            if (data[i]['isSelected'] == true) {
                              print(i);
                              item_id_added.add(widget
                                  .show_orders_items_dialog[i].id
                                  .toString());
                            }
                            *//*if(data[i]['isSelected'] = true){
                                      item_id = widget
                                          .show_orders_items_dialog[i]
                                          .id.toString();
                                    }*//*
                          }

                          KitchenRepository()
                              .fullfillItem(item_id_added, widget.order_id,
                                  employee_id, restaurant_id)
                              .then((result) {
                            print("ORDERFULLFILL" + result.toString());
                            Toast.show("FULLFILLED SUCCESSFULLY", context,
                                duration: Toast.LENGTH_SHORT,
                                gravity: Toast.BOTTOM);
                            if (result.responseStatus == 0) {
                              *//*_loading = false;
        Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();*//*
                              Toast.show("No Data Found", context,
                                  duration: Toast.LENGTH_SHORT,
                                  gravity: Toast.BOTTOM);
                            } else {
                              setState(() {
                                _loading = false;
                                Navigator.pop(context, "Cancelled");
                                *//*Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => HomePage()),
                                );*//*
                              });
                            }
                          });
                        },
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    login_passcode_bg1,
                                    login_passcode_bg2
                                  ]),
                              borderRadius: BorderRadius.circular(0)),
                          child: Text("FULFILL",
                              style: TextStyle(
                                  fontSize: SizeConfig.safeBlockHorizontal * 4,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w800,
                                  color: Colors.white)),
                        ))),
                Container(
                    margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                    height: 50,
                    child: FlatButton(
                        onPressed: () {
                          KitchenRepository()
                              .unfullfillItem(item_id_added, widget.order_id,
                                  employee_id, restaurant_id)
                              .then((result) {
                            print("ORDERUNFULLFILL" + result.toString());
                            Toast.show("UNFULLFILLED SUCCESSFULLY", context,
                                duration: Toast.LENGTH_SHORT,
                                gravity: Toast.BOTTOM);
                            if (result.responseStatus == 0) {
                              Toast.show("No Data Found", context,
                                  duration: Toast.LENGTH_SHORT,
                                  gravity: Toast.BOTTOM);
                            } else {
                              setState(() {
                                _loading = false;
                                Navigator.pop(context, "Cancelled");
                              });
                            }
                          });
                          Navigator.pop(context, "Cancelled");
                          Toast.show("UN FULFILLED", context,
                              duration: Toast.LENGTH_SHORT,
                              gravity: Toast.BOTTOM);
                        },
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    login_passcode_bg1,
                                    login_passcode_bg2
                                  ]),
                              borderRadius: BorderRadius.circular(0)),
                          child: Text("UN FULFILL",
                              style: TextStyle(
                                  fontSize: SizeConfig.safeBlockHorizontal * 4,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w800,
                                  color: Colors.white)),
                        ))),
              ],
            )))
          ],
        )));
  }
}*/

DateTimeConverter(String createddate) {
  print("CREATEDDATE" + createddate.toString());
  var now = DateFormat('E, d MMM yyyy HH:mm:ss').parse(createddate);
  String formattedTime = DateFormat.Hm().format(now);
  print("DATETIME======" + formattedTime);
  return formattedTime.toString();
}
