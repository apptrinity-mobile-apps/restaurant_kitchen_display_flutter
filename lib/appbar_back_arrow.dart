import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restaurant_kitchendisplay/utils/all_constans.dart';



class YourAppbarBackArrow extends StatelessWidget implements PreferredSizeWidget {

  final int count;
  final String title_text;

  const YourAppbarBackArrow({
    Key key,
    this.count,
    this.title_text,
  }) : super(key: key);
  @override
  Size get preferredSize =>  Size.fromHeight(70);

  @override
  Widget build(BuildContext context) {

    return
      Container(color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            AppBar(
              toolbarHeight:  preferredSize.height,
              automaticallyImplyLeading: false,
              elevation: 0.0,
              backgroundColor: Colors.white,
              centerTitle: false,
              title: Text(
                  title_text,style: new TextStyle(
                  color: login_passcode_text,
                  fontSize: 20.0,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600)

              ),
              leading: Builder(
                builder: (BuildContext context) {
                  return IconButton(
                    padding: EdgeInsets.only(left: 12.0),
                    icon: Image.asset("images/back_arrow.png", width: 20, height: 20),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  );
                },
              ),
              actions: [
              ],
            ),
          ],
        ),

      );
  }
}
